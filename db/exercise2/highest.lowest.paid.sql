SELECT department.department_name 
      ,MIN(annual_salary) AS least_paid
      ,MAX(annual_salary) AS highest_paid
  FROM data.employee 
	  ,data.department
 WHERE department.department_number = employee.department_number
 GROUP BY department.department_number;