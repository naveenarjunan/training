CREATE TABLE department(
	dept_no int PRIMARY KEY,
	dept_name varchar(25));
CREATE TABLE employee(
	emp_id int PRIMARY KEY,
	first_name varchar(25),
	surname varchar(25),
	dob varchar(25),
	date_of_joining varchar(25),
	annual_salary int,
	dept_no int,
	FOREIGN KEY(dept_no) REFERENCES department(dept_no));
INSERT INTO department(dept_no,dept_name)
VALUES(1,'ITDesk');
      (2,'Finance');
      (3,'Engineering');
      (4,'HR');
      (5,'Recruiment');
      (6,'Facility');
INSERT INTO employee(emp_id,first_name ,surname,dob,date_of_joining,annual_salary,dept_no)
VALUES(1,'sunil','gokul','2/02/2001','4/05/2021',60000000,1);
      (2,'kavin','gokul','4/05/2001','7/05/2021',77787870,2);
      (3,'kumar','raj','19/03/2001','9/05/2021',6545445,5);
      (4,'hulk','bruce','22/06/2001','1/06/2021',645454545,4);
      (5,'tom','kent','4/09/2001','6/07/2021',654545421,3);
      (6,'jerry','jack','18/10/2001','4/07/2021',60454210,1);
      (7,'dev','stark','11/1/2001','27/08/2021',60542010,6);
      (8,'krish','tonny','11/12/2001','17/09/2021',42015,3);
      (9,'kanan','vel','15/11/2001','4/10/2021',60005454,2);
      (10,'ravi','vijay','1/01/2001','1/11/2021',604540,4);
      (11,'bob','buddy','2/02/2001','4/05/2021',60000000,1);
      (12,'karthi','natasha','4/05/2001','7/05/2021',77787870,2);
      (13,'suriya','narayanan','19/03/2001','9/05/2021',6545445,5);
      (14,'arun','aravindh','22/06/2001','1/06/2021',645454545,4);
      (15,'cholan','aadhi','4/09/2001','6/07/2021',654545421,3);
      (16,'arjun','karnan','18/10/2001','4/07/2021',60454210,1);
      (17,'sanjay','rathesh','11/1/2001','27/08/2021',60542010,6);
      (18,'vishnu','tharun','11/12/2001','17/09/2021',42015,3);
      (19,'vijayan','kumaran','15/11/2001','4/10/2021',60005454,2);
      (20,'saran','jayanth','1/01/2001','1/11/2021',604540,4);
      (21,'murugapan','mani','2/02/2001','4/05/2021',60000000,1);
      (22,'vadivelu','madhavn','4/05/2001','7/05/2021',77787870,2);
      (23,'vivek','sendhil','19/03/2001','9/05/2021',6545445,5);
      (24,'barathi','veelan','22/06/2001','1/06/2021',645454545,4);
      (25,'pandi','sathyan','4/09/2001','6/07/2021',654545421,3);
      (26,'akash','karan','18/10/2001','4/07/2021',60454210,6);
      (27,'thanesh','suresh','11/1/2001','27/08/2021',60542010,6);
      (28,'surandar','sukumar','11/12/2001','17/09/2021',42015,5);
      (29,'rasul','rahman','15/11/2001','4/10/2021',60005454,6);
      (30,'ram','ranjith','1/01/2001','1/11/2021',604540,5);

SELECT e.first_name
	  ,d.dept_name 
  FROM employee e 
      INNER JOIN  department d
			ON (d.dept_no = e.dept_no);

SELECT AVG(annual_salary)
	FROM employee;

SELECT department.department_name 
      ,MIN(annual_salary) AS least_paid
      ,MAX(annual_salary) AS highest_paid
  FROM data.employee 
      ,data.department
WHERE department.department_number = employee.department_number
GROUP BY department.department_number;

UPDATE coach 
	SET salary=salary+(salary*10/100);	  
	  
	  SELECT emp_id
		     ,first_name
			 ,dob FROM employee
  WHERE dept IS NULL;
  
  
SELECT employee.first_name
      ,department.name 
	  ,employee.area
  FROM employee employee
      ,department department 
 WHERE employee.department_id=department.id 
  AND (employee.area IN('coimbatore') AND department.name IN('Recruitment'));