CREATE TABLE employee(
	emp_id int PRIMARY KEY,
	first_name varchar(25),
	surname varchar(25),
	dob varchar(25),
	date_of_joining varchar(25),
	annual_salary int,dept_no int);
INSERT INTO employee(emp_id,first_name ,surname,dob,date_of_joining,annual_salary,dept_no) 
VALUES(1,'naveen','kumar','2/02/2001','4/05/2021',60000000,1);
      (2,'kavin','raj','4/05/2001','7/05/2021',77787870,2);
      (3,'bhuvan','raj','19/03/2001','9/05/2021',6545445,5);
      (4,'sai','kumar','22/06/2001','1/06/2021',645454545,4);
      (5,'praveen','kumar','4/09/2001','6/07/2021',654545421,3);

SELECT emp_id
	   ,first_name
	   ,surname
	   ,dob
	   ,date_of_joining
	   annual_salary
	   ,dept_no
  FROM employee
	WHERE dob='4/09/2001'