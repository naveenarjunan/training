CREATE TABLE employee(
	emp_id varchar(5) NOT NULL,
	first_name varchar(20) NULL,
	dob VARCHAR(25),
	annual_salary int,
	emp_supv varchar(5) NULL,
	PRIMARY KEY(emp_id) ,
	FOREIGN KEY(emp_supv) REFERENCES employee(emp_id));
INSERT INTO employee(emp_id,first_name ,dob,annual_salary) 
	VALUES(1,'naveen','2/02/2001',60000000);
	      (2,'kavin','4/05/2001',77787870,1);
	      (3,'bhuvan','19/03/2001',6545445,1);
	      (4,'sai','22/06/2001',645454545,2);
	      (5,'praveen','4/09/2001',654545421,2);

SELECT a.emp_id AS "Emp_ID"
		,a.first_name AS "Employee Name"
		,c.emp_id AS "Supervisor ID"
		,c.first_name AS "Supervisor Name"
  FROM employee a, employee c
WHERE a.emp_supv = c.emp_id;
