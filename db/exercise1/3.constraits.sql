CREATE TABLE coach(
	coach_id int NOT NULL KEY,
	name varchar(25),
	age varchar(25),
	city varchar(25),
	game varchar(25) default 'football');
INSERT INTO coach(coach_id,name ,age,city) 
	values(1,'naveen','47','salem');
          (2,'kavin','38','madurai');
          (3,'praveen','29','covai');

CREATE TABLE player(
	player_id int NOT NULL  PRIMARY KEY,
	name varchar(25) ,
	age int check(age>15),
	coach_id int,
	city varchar(25),
	FOREIGN KEY(coach_id) REFERENCES coach(coach_id));
INSERT INTO player(player_id,name ,age,city) 
	values(1,'sai','58','tiruppur');
	      (2,'scott','17','theni');
CREATE INDEX idx_pl
ON Player (player_id);
