CREATE TABLE `db`.`department` (
             `department_id` INT NOT NULL
            ,`department` VARCHAR(45) NOT NULL
            ,PRIMARY KEY (`department_id`));
CREATE TABLE `db`.`employee` (
             `employee_id` INT NOT NULL
            ,`first_name` VARCHAR(45) NOT NULL
			,`surname` VARCHAR(45) NOT NULL
            ,`age` INT NOT NULL
            ,`city` VARCHAR(45) NOT NULL
            ,`salary` INT NOT NULL
            ,`department_id` INT NOT NULL
			,PRIMARY KEY (`employee_id`)
            ,INDEX `department_id_idx` (`department_id` ASC) VISIBLE
            ,CONSTRAINT `department_id`
     FOREIGN KEY (`department_id`)
  REFERENCES `db`.`department` (`department_id`)
   ON DELETE RESTRICT
   ON UPDATE CASCADE);
INSERT INTO db.department (department_id, department) 
	 VALUES ('1', 'Electrical');
	        ('2', 'Instrumentation');
INSERT INTO db.employee (employee_id, first_name, surname, age, city, department_id,salary) 
	 VALUES ('1', 'naveen','V', '20', 'Salem', '1','550000');
	        ('2', 'arjun','P', '21', 'madhurai', '2','600000');
	        ('3', 'Venkat','R', '19', 'erode', '1','650000');
	        ('4', 'karthi','M', '22', 'gobi', '2','700000');
	        ('5', 'krish','G', '20', 'covai', '2','630000');
     SELECT employee_id
           ,first_name
           ,department
           ,employee.department_id
	  FROM db.employee,db.department
     WHERE db.employee.department_id = db.department.department_id;
    SELECT employee_id
          ,first_name
          ,department_id
	  FROM  db.employee
     WHERE department_id =any (select department_id from db.department where department='Instrumentation');