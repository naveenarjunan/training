ALTER TABLE coach 
	ADD name varchar(25);
ALTER TABLE coach 
	RENAME column name to first_name;
ALTER TABLE coach 
	DROP first_name;