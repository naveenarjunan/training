SELECT db.employee.employee_id
      ,db.department.department
      ,db.project_detail.project
      ,db.department.department_id
  FROM db.employee
      INNER JOIN db.department 
              ON db.employee.department_id = db.department.department_id
      INNER JOIN db.project_detail 
              ON db.employee.employee_id  = db.project_detail.employee_id;
SELECT db.employee.employee_id
      ,db.department.department
      ,db.project_detail.project
      ,db.department.department_id
  FROM db.employee
      CROSS JOIN db.department 
              ON db.employee.department_id = db.department.department_id
      CROSS JOIN db.project_detail 
              ON db.employee.employee_id  = db.project_detail.employee_id; 
SELECT db.employee.employee_id
      ,db.department.department
      ,db.project_detail.project
      ,db.department.department_id
  FROM db.employee
      LEFT JOIN db.department 
             ON db.employee.department_id = db.department.department_id
      LEFT JOIN db.project_detail 
             ON db.employee.employee_id  = db.project_detail.employee_id;
SELECT db.employee.employee_id
      ,db.department.department
      ,db.project_detail.project
      ,db.department.department_id
  FROM db.employee
       RIGHT JOIN db.department 
              ON db.employee.department_id = db.department.department_id
       RIGHT JOIN db.project_detail 
              ON db.employee.employee_id  = db.project_detail.employee_id;
