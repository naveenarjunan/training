SELECT doctor_id
      ,name
      ,age
      ,specialization
      ,experience
      ,city
  FROM hospital.doctor
 WHERE experience = '25' 
   AND specialization = 'Allergist';
SELECT doctor_id
      ,name
      ,age
      ,specialization
      ,experience
      ,city
  FROM hospital.doctor
 WHERE age = '30' 
    OR city = 'madurai';
SELECT doctor_id
      ,name
      ,age
      ,specialization
      ,experience
      ,city
  FROM hospital.doctor
 WHERE doctor_id IN ('1', '2', '3');
SELECT doctor_id
      ,name
      ,age
      ,specialization
      ,experience
      ,city
  FROM hospital.doctor
 WHERE NOT age = '50';
SELECT doctor_id
      ,name
      ,age
      ,specialization
      ,experience
      ,city
  FROM hospital.doctor
 WHERE name LIKE 's%';
SELECT doctor_id
      ,name
      ,age
      ,specialization
      ,experience
      ,city
  FROM hospital.doctor
 WHERE name LIKE '%n';
SELECT doctor_id
      ,name
      ,age
      ,specialization
      ,experience
      ,city
  FROM hospital.doctor
 WHERE age LIKE '3%5';
SELECT doctor_id
      ,name
      ,age
      ,specialization
      ,experience
      ,city
  FROM hospital.doctor
 WHERE name LIKE 's_i';
SELECT name
      ,age
      ,specialization
  FROM hospital.doctor
 WHERE dept_id = ANY (SELECT dept_id 
                        FROM hospital.department 
                       WHERE dept_id = '2');