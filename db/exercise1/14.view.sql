CREATE VIEW doctorview AS
     SELECT name
           ,salary
       FROM doctor
      WHERE salary IS NOT NULL
      WITH CHECK OPTION;
CREATE VIEW adminview AS
     SELECT doctor_id
           ,name
           ,specialization
           ,experience
           ,dept_id
       FROM doctor;
