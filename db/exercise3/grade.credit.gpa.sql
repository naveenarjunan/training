SELECT roll_number
      ,student.name
      ,dob
      ,gender
      ,email
      ,phone
      ,address
      ,academic_year
      ,semester
      ,grade
      ,credits
      ,gpa
      ,college.name
      ,dept_name
  FROM student
      INNER JOIN semester_result 
	     ON semester_result.stud_id = student.id
      INNER JOIN college 
	     ON college.id = student.college_id
      INNER JOIN college_department 
	     ON college_department.cdept_id = student.cdept_id
      INNER JOIN department 
	     ON department.dept_code = college_department.udept_code
      ORDER BY college.name,semester_result.semester;