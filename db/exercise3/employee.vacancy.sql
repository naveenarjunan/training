 SELECT designation.name
       ,designation.rank
       ,college.name
       ,department.dept_name
       ,university.university_name
   FROM employee
     LEFT JOIN college
            ON college.id = employee.college_id
     LEFT JOIN college_department 
            ON college_department.cdept_id = employee.cdept_id
           AND college_department.college_id = college.id
     LEFT JOIN designation
            ON designation.id = employee.desig_id
     LEFT JOIN department
            ON department.dept_code = college_department.udept_code
     INNER JOIN university
            ON university.univ_code = college.univ_code
           AND employee.name = ''
      ORDER BY designation.rank