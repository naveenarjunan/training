SELECT employee.name
      ,dob
      ,email
      ,phone
      ,college.name
      ,city
      ,state
      ,year_opened
      ,dept_name
      ,designation.name
  FROM employee
      INNER JOIN college 
              ON college.id = employee.college_id
      INNER JOIN designation 
              ON designation.id = employee.desig_id
       LEFT JOIN college_department 
              ON college_department.cdept_id = employee.cdept_id
       LEFT JOIN department 
              ON department.dept_code = college_department.udept_code
      INNER JOIN university 
              ON university.univ_code = college.univ_code
                      AND university_name = 'Anna University'
   ORDER BY college.name, designation.rank;