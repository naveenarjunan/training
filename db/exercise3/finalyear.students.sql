SELECT roll_number AS ROLL_NUMBER
      ,student.name AS NAME
      ,gender AS GENDER
      ,dob AS DOB
      ,email AS EMAIL
      ,phone AS PHONE
      ,address AS ADDRESS
      ,college.name AS COLLEGE_NAME
      ,dept_name AS DEPARTMENT_NAME
  FROM student
      INNER JOIN college 
	     ON college.id = student.college_id
                   AND (city = 'Coimbatore' AND academic_year = '2018')
      INNER JOIN University 
	     ON university.univ_code = college.univ_code
                   AND college.id = student.college_id
                   AND university_name = 'Anna University'
      INNER JOIN college_department 
	     ON college_department.cdept_id = student.cdept_id
      INNER JOIN department
	     ON department.dept_code = college_department.udept_code;