SELECT university.university_name        
       ,college.name AS COLLEGE_NAME
       ,semester
       ,sum(amount) AS collected
  FROM semester_fee 
       INNER JOIN student 
	      ON student.id = semester_fee.stud_id 
       INNER JOIN college 
	      ON college.id = student.college_id
       INNER JOIN university 
	      ON university.univ_code = college.univ_code 
		      AND semester_fee.paid_status = 'paid' 
       GROUP BY college.name,semester_fee.semester;

SELECT  university.university_name        
       ,college.name AS COLLEGE_NAME
       ,semester
       ,sum(amount) AS uncollected
  FROM semester_fee 
       INNER JOIN student 
	      ON student.id = semester_fee.stud_id 
       INNER JOIN college 
	      ON college.id = student.college_id
       INNER JOIN university
	      ON university.univ_code = college.univ_code  
		     AND semester_fee.paid_status = 'un_paid'
       GROUP BY college.name,semester_fee.semester;
SELECT university.university_name        
       ,semester
       ,sum(amount) AS collected
  FROM semester_fee 
       INNER JOIN student 
	      ON student.id = semester_fee.stud_id 
       INNER JOIN college 
	      ON college.id = student.college_id
       INNER JOIN university 
	      ON university.univ_code = college.univ_code 
		     AND semester_fee.paid_status = 'paid' 
       GROUP BY university.university_name;