
/*
 * Requirements:
 *    1) To write a Java program to copy all of the mappings from the specified map to another map?
 *    2) To write a Java program to test if a map contains a mapping for the specified key?
 *    3) Count the size of mappings in a map?
 *    4) To Write a Java program to get the portion of a map whose keys range from a given key to another key?
 
 * Entities:
 *    MapOperation
 
 * Function Declaration:
 *    public static void main(String[] args) 
 
 * Jobs To Be Done:
 *    1.create the object map as generic type.
 *    2.Put the keys and the values are in map, prints the map.
 *    3.create the another object map1 as Generic type.
 *       3.1) map values are copy to map1 Using putAll() method.
 *       3.2) Print the output of map1, size of the map1. 
 *       3.3) get the values using get(key) method. 
         3.4) particular ranges between the keys, values using subMap(). 
         
Pseudo code: 
public class MapOperation {

	public static void main(String[] args) {
		HashMap<Integer, String> map = new HashMap<>();
		//add the elements.
		System.out.println(map);

		TreeMap<Integer, String> map1 = new TreeMap<>();
		map1.putAll(map);
		System.out.println(map1);
		System.out.println(map1.size());
		System.out.println(map1.get(3));
		System.out.println(map1.subMap(2, 4));

	}
}
*/

package com.java.training.exercise3.map;

import java.util.HashMap;
import java.util.TreeMap;

public class MapOperation {

	public static void main(String[] args) {

		HashMap<Integer, String> map = new HashMap<>();
		map.put(1, "one");
		map.put(2, "two");
		map.put(3, "three");
		map.put(4, "four");
		map.put(5, "five");
		System.out.println(map);

		TreeMap<Integer, String> map1 = new TreeMap<>();
		map1.putAll(map);
		System.out.println(map1);
		System.out.println(map1.size());
		System.out.println(map1.get(3));
		System.out.println(map1.subMap(2, 4));

	}
}



