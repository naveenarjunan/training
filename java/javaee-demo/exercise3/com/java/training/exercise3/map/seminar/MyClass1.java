package com.java.training.exercise3.map.seminar;

import java.util.*;

public class MyClass1 {
	public static void main(String[] args) {

		HashMap<String, String> capitalCities = new HashMap<String, String>();

		//enter entries
		capitalCities.put("England", "London");
		capitalCities.put("Germany", "Berlin");
		System.out.println(capitalCities.get("England"));

		//remove entries
		//capitalCities.remove("Germany");
		//System.out.println(capitalCities);

		//to print the size of map
		System.out.println(capitalCities.size());

		//Check if Map Contains Key
		boolean hasKey = capitalCities.containsKey("England");
		System.out.println(hasKey);       

		//Check if Map Contains values
		boolean hasValue = capitalCities.containsValue("Boston");
		System.out.println(hasValue);       

		// remap the values using compute() method 
		capitalCities.compute("England", (key, value) -> value.concat("  English")); 
		capitalCities.compute("Germany", (key, value) -> value.concat("  German"));
		System.out.println(capitalCities);

		//to remove all the entries
		capitalCities.clear();
		System.out.println(capitalCities);
	}
}