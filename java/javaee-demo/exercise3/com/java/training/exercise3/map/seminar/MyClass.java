package com.java.training.exercise3.map.seminar;

import java.util.HashMap;

public class MyClass {
	public static void main(String[] args) {
		HashMap<String, String> capitalCities = new HashMap<String, String>();
		capitalCities.put("England", "London");
		capitalCities.put("Germany", "Berlin");
		capitalCities.put("Norway", "Oslo");
		capitalCities.put("USA", "Washington DC");

		//print keys
		for (String i : capitalCities.keySet()) {
			System.out.println(i);
		}
		//print values
		for (String i : capitalCities.values()) {
			System.out.println(i);
		}
		// Print keys and values
		for (String i : capitalCities.keySet()) {
			System.out.println("\nkey: " + i+"      value: " + capitalCities.get(i));
		}
	}
}