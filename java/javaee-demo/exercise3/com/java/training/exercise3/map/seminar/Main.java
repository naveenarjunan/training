package com.java.training.exercise3.map.seminar;

import java.util.*; 

public class Main { 

	public static void main(String[] args) { 

		// Create a Treemap and inserting elements 
		TreeMap<Integer, String> sortedmap = new TreeMap<Integer, String>(); 

		// Adding Entries to map
		sortedmap.put(2, "Two");
		sortedmap.put(4, "Four"); 
		sortedmap.put(1, "One"); 
		sortedmap.put(5, "Five"); 
		sortedmap.put(3, "Three"); 
		System.out.print("The sortedMap is : "+ sortedmap);

		// Displaying the firstkey
		System.out.print("\nThe firstkey  is : "+ sortedmap.firstKey());

		// Displaying the lastkey
		System.out.print("\nThe lastkey  is : "+ sortedmap.lastKey());

		// Displaying the headmap
		System.out.print("\nThe HeadMap is : "+ sortedmap.headMap(3));

		// Displaying the tailmap 
		System.out.println("\nThe TailMap is " + sortedmap.tailMap(4)); 

		// Displaying the submap
		System.out.println("The SubMap is " + sortedmap.subMap(2,4));
	} 
} 