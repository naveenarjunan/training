package com.java.training.exercise3.map.seminar;

import java.util.SortedMap;
import java.util.TreeMap;

public class SortedMapDemo { 
	public static void main(String[] args) 
	{ 
		// Create a TreeSet and inserting elements 
		SortedMap<Integer, String> mp = new TreeMap<>(); 

		// Adding Element to SortedSet 
		mp.put(1, "One"); 
		mp.put(2, "Two"); 
		mp.put(3, "Three"); 
		mp.put(4, "Four"); 
		mp.put(5, "Five"); 

		// Returning the map with key less than 3 
		System.out.print("Last Key in the map is : "
				+ mp.headMap(3)); 
	} 
} 