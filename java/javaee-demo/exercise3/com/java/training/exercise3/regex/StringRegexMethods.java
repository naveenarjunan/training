/*
Requirement:
	To write a program for Java String Regex Methods
Entity:
	StringRegexMethods
Function Declaration:
	public static void main(String[] args) {}
Jobs To Be Done:
	    1)Declare a String named text and store string in it.
 * 		2)use matches methods
 *        2.1)check whether the string matches and store the result as boolean
 *      3)if string is matched print match found.
 *        3.1)otherwise print as match not found  
 *      4)use split method and store the splitted string in string array
 *      5)print the string array.
 *      6)use replaceFirst method and store the replaced string
 *      7)print the replaced string
 *      8)use replaceAll method and store the replaced string.
 *      9)print the replaced string.
Pseudo code:
	public class StringRegexMethods {

		public static void main(String[] args) {

			String text = "hi i am steve and i am from america";
			// use match() method to match name

			// use split() method and split into two substrings
			// print the result

			// use replaceFirst() method and replace the first matching one in the text
			// print the result

			// use replaceAll() method to replace all the string matches with the given one
			// print the result
		}
	}
 */

package com.java.training.exercise3.regex;

import java.util.Arrays;

public class StringRegexMethods {

	public static void main(String[] args) {

		String text = "Hello there and i welcome you all";
		boolean matches = text.matches(".*you.*");
		System.out.println(matches);

		String[] string = text.split("and");
		System.out.println(Arrays.toString(string));

		String s = text.replaceFirst("i", "we");
		System.out.println(s);

		String t = s.replaceAll("you all", "you");
		System.out.println(t);
	}
}