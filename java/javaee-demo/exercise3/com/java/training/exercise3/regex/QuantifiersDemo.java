/*Requirement:
	write a program for java regex quantifier.
	
 Entity:
 	QuantifierDemo
 
 Function Declaration
	public static void main(String[] args)
	
 Jobs To Be Done:
         1)Declare a String named text an store string value in it.
         2)Create a regex pattern named pattern to print 0 or more occurance of a.
         3)create a Matcher named matcher and store the match of the pattern against the text
         4)For each match
             4.1)Print the starting and ending index.
         5)Create a regex pattern named pattern1 to print 0 or one occurance of a.
         6)create a Matcher named matcher1 and store the match of the pattern1 against the text.
         7)For each match
             7.1)Print the starting and ending index.
         8)Create a regex pattern named pattern2 to print one or more occurance of a.
         9)create a Matcher named matcher2 and store the match of the pattern2 against the text.
         10)For each match
             10.1)Print the starting and ending index.
         11)Create a regex pattern named pattern3 to print exactly 2 times occurance of a.
         12)create a Matcher named matcher3 and store the match of the pattern3 against the text.
         13)For each match
             13.1)Print the starting and ending index.
         14)Create a regex pattern named pattern4 to print At-least 2 occurrences of a
         15)create a Matcher named matcher4 and store the match of the pattern4 against the text.
         16)For each match
             16.1)Print the starting and ending index.
         17)Create a regex pattern named pattern5 to print the count of occurrences of a is from 2 to 5.
         18)create a Matcher named matcher5 and store the match of the pattern5 against the text.
         19)For each match
             19.1)Print the starting and ending index.
 *    
 *pseudo code:
 public class QuantiferDemo {
	
	public static void main(String[] args) {
		
		String text = "appleaarweaaaaaaaaaaarrrrrrrrrrrsasasasasasasassassa";

		Pattern pattern = Pattern.compile("a*");
		Matcher matcher = pattern.matcher(text);
		while (matcher.find()) {
			System.out.println("Pattern found from " + matcher.start() + 
	                " to " + (matcher.end()));
		}
		
		Pattern pattern1 = Pattern.compile("a+");
		Matcher matcher1 = pattern1.matcher(text);
		while (matcher1.find()) {
			System.out.println("Pattern found from " + matcher1.start() + 
	                " to " + (matcher1.end()));
		}
		
		Pattern pattern2 = Pattern.compile("a?");
		Matcher matcher2 = pattern2.matcher(text);
		while (matcher2.find()) {
			System.out.println("Pattern found from " + matcher2.start() + 
	                " to " + (matcher2.end()));
		}
		
		Pattern pattern3 = Pattern.compile("a{2}");
		Matcher matcher3 = pattern3.matcher(text);
		while (matcher3.find()) {
			System.out.println("Pattern found from " + matcher3.start() + 
	                " to " + (matcher3.end()));
		}
		
		Pattern pattern4 = Pattern.compile("a{2,}");
		Matcher matcher4 = pattern4.matcher(text);
		while (matcher4.find()) {
			System.out.println("Pattern found from " + matcher4.start() + 
	                " to " + (matcher4.end()));
		}
		
		Pattern pattern5 = Pattern.compile("a{2,5}");
		Matcher matcher5 = pattern5.matcher(text);
		while (matcher5.find()) {
			System.out.println("Pattern found from " + matcher5.start() + 
	                " to " + (matcher5.end()));
		}
	}
}
 *
 *
 * */
package com.java.training.exercise3.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class QuantifiersDemo {

	public static void main(String[] args) {

		String text = "angerrrrraaaaaaarrraararrrrarrrrrraaaaa";

		Pattern pattern = Pattern.compile("a*");
		Matcher matcher = pattern.matcher(text);
		System.out.println("Quantifier - *");
		while (matcher.find()) {
			System.out.println("Pattern found from " + matcher.start() + 
					" to " + (matcher.end()));
		}

		Pattern pattern1 = Pattern.compile("a+");
		Matcher matcher1 = pattern1.matcher(text);
		System.out.println("Quantifier - +");
		while (matcher1.find()) {
			System.out.println("Pattern found from " + matcher1.start() + 
					" to " + (matcher1.end()));
		}

		Pattern pattern2 = Pattern.compile("a?");
		Matcher matcher2 = pattern2.matcher(text);
		System.out.println("Quantifier - ?");
		while (matcher2.find()) {
			System.out.println("Pattern found from " + matcher2.start() + 
					" to " + (matcher2.end()));
		}

		Pattern pattern3 = Pattern.compile("a{2}");
		Matcher matcher3 = pattern3.matcher(text);
		System.out.println("Quantifier -{2}");
		while (matcher3.find()) {
			System.out.println("Pattern found from " + matcher3.start() + 
					" to " + (matcher3.end()));
		}

		Pattern pattern4 = Pattern.compile("a{2,}");
		Matcher matcher4 = pattern4.matcher(text);
		System.out.println("Quantifier - {2,}");
		while (matcher4.find()) {
			System.out.println("Pattern found from " + matcher4.start() + 
					" to " + (matcher4.end()));
		}

		Pattern pattern5 = Pattern.compile("a{2,5}");
		Matcher matcher5 = pattern5.matcher(text);

		System.out.println(" Quantifier - {2,5}");
		while (matcher5.find()) {
			System.out.println("Pattern found from " + matcher5.start() + 
					" to " + (matcher5.end()));
		}
	}
}