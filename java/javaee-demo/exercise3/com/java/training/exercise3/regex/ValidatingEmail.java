/*
 * Requirement:
 *      write a program for email-validation?
 *Entity:
 *      ValidatingEmail
 * Function Declaration:
 *      public static void main(String[] args)
 * Jobs to Done:
 *      1) Create class named as ValidatingEmail.
 *      2) use scanner and get the input from the user.
 *      3) Give the condtions in the string object.
 *      4) use the matches() method to check the conditions.
 *      5) if the given email satisfies the given condition, returns true else false.
 *      
 *pseudo code:
 
 	public class ValidatingEmail {
 
   		public static void main(String[] args) {
   
      		Scanner scanner = new Scanner(System.in);
      		//get the input from the user
      		String regex = "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$";
      		boolean result = mail.matches(regex);
      		//if matches print valid else print not valid using if condition
   		}
	}

 *      
 */package com.java.training.exercise3.regex;

 import java.util.Scanner;

 public class ValidatingEmail {

	 public static void main(String[] args) {

		 Scanner scanner = new Scanner(System.in);
		 System.out.println("Enter your Email: ");
		 String mail = scanner.next();
		 String regex = "^[a-z0-9+_.-]+@[a-z.-]+$";
		 boolean result = mail.matches(regex);
		 scanner.close();

		 if(result) {
			 System.out.println("Given email-id is valid");
		 } else {
			 System.out.println("Given email-id is not valid");
		 }
	 }
 }
