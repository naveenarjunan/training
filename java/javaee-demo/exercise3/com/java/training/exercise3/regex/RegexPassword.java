/*
 Requirement :
   create a pattern for password which contains
   8 to 15 characters in length
   Must have at least one upper case letter
   Must have at least one lower case letter
   Must have at least one digit

Entity:
    RegexPassword
    
Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1)Create a Scanner named scanner
    2)Get the password as input from the user and store it in userName
    3)Invoke the method validPassword and store it in result.
      3.1)Create a regex pattern for password and store it in pattern of type Pattern.
      3.2)Create a Matcher named matcher and it matches the password  with pattern
      3.3)If the pattern matches it will return true otherwise it will return false
    4)Check whether the result is true
      4.1)Print "password is valid"
      4.2)Otherwise print "password is invalid"
	
pseudo code:
public class PasswordDemo {

    public static boolean ValidPassword(String password) {
        regex
        // using regex (0-9)(a-z)(A-Z){8,15}$

        Pattern pattern
        // use pattern
        if (password == null) {
            return false;
        }
        
        Matcher matcher
        \\use matcher
        return matcher.matches();
    }

    public static void main(String[] args) {
        Scanner scanner
        // use scanner to get input
        System.out.println("Enter the Password ");
        String password = sc.next();
        System.out.println(ValidPassword(password));
        sc.close();
    }
}
  
 */
package com.java.training.exercise3.regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexPassword {

	public static boolean validPassword(String password) {
		String word = "^(?=.*[0-9])" + "(?=.*[a-z])(?=.*[A-Z])" + "(?=\\S+$).{8,12}$";

		Pattern pattern = Pattern.compile(word);
		if (password == null) {
			return false;
		}

		Matcher matcher = pattern.matcher(password);
		return matcher.matches();
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the Password ");
		String password = scanner.next();
		if (validPassword(password))
			System.out.print("password is valid");
		else
			System.out.print("Password is invalid");
		scanner.close();
	}
}