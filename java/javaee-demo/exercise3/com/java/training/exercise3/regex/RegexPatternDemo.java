/*
 * Requirement:
 *       create a pattern for name which contains
 *       6 characters in length.
 *       Must start with "M". 
 *Entity:
 *     RegexPatternDemo
 * Function Declaration:
 *      public static boolean Validname(String name)
 *      public static void main(String[] args)
 * Jobs to Done:
 *      1) Create package as regex and import Scanner,Matcher and Pattern package  in it.
 *      2) Create class named as RegexDemo and create Validname in it.
 *      3) Give the condtions in the string object
 *      4) By using Pattern compile the regex.
 *      5) Under the main method the name is given at the run time by using scanner.
 *      6) if the name satisfy the given condition it returns true else false. 
 *      
 */
package com.java.training.exercise3.regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexPatternDemo {

	public static boolean Validname(String name) {
		String regex = "^(?=.*[0-9])" + "(?=.*[a-z])(?=.*[A-Z])" + "(?=\\S+$).{8,15}$";

		Pattern pattern = Pattern.compile(regex);
		if (name == null) {
			return false;
		}
		Matcher matcher = pattern.matcher(name);
		return matcher.matches();
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the name ");
		String name = sc.next();
		System.out.println(Validname(name));
		sc.close();
	}
}
