/*
 * Requirement:
 *      create a username for login website which contains
 *      8-12 characters in length
 *      Must have at least one uppercase letter
 *      Must have at least one lower case letter
 *      Must have at least one digit
 *      
 *Entity:
 *     RegexUsername
 *     
 * Function Declaration:
 *      public static boolean validUserName(String UserName);
 *      public static void main(String[] args)
 *      
 * Jobs to Done:
 *     1)Create a Scanner named scanner
       2)Get the userName as input from the user and store it in userName
       3)Invoke the method validatePassword and store it in result.
         3.1)Create a regex pattern for userName and store it in pattern of type Pattern.
         3.2)Create a Matcher named matcher and it matches the userName  with pattern
         3.3)If the pattern matches it will return true otherwise it will return false
       4)Check whether the result is true
         4.1)Print "username is valid"
         4.2)Otherwise print "username is invalid"
 *         
 * Pseudocode:
 *     
   class RegexUsername {

      public static boolean validUserName(String userName) {
          String regex = "^(?=.*[0-9])" + "(?=.*[a-z])(?=.*[A-Z])" + "(?=\\S+$).{8,12}$";
          //Pattern pattern or String pattern
          if (input != pattern)
              return false;
          else
              return true;
          }
          public static void main(String[] args) {
          Scanner scanner = new Scanner(System.in);
          String userName = scanner.next();
          System.out.println(validUserName(userName));
          }
    }

 */
package com.java.training.exercise3.regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUsername {

	public static boolean validUserName(String userName) {
		String regex = "^(?=.*[0-9])" + "(?=.*[a-z])(?=.*[A-Z])" + "(?=\\S+$).{8,12}$";

		Pattern pattern = Pattern.compile(regex);
		if (userName == null) {
			return false;
		}
		Matcher matcher = pattern.matcher(userName);
		return matcher.matches();
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the UserName ");
		String userName = scanner.next();
		if (validUserName(userName))
			System.out.print("Username is valid");
		else
			System.out.print("Username is invalid");
		scanner.close();
	}
}
