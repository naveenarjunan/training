/*
Requirement:
	For the following code use the split method() and print in sentence
    String website = "https-www-google-com";

Entity:
	SplitMethod
Function Declaration:
	public static void main(String[] args) {}
Jobs To Be Done:
	1.Declare the variable website and store "https-www-google-com".
	2.Using split() method  remove the "-".
	3.Print the result in sentence.
Pseudo code:
	public class SplitMethod {
	
	public static void main(String[] args) {
		
			String website = "https-www-google-com";
			// use split() method to split the given string
			// print result using for each loop
		}
	}
	
 */

package com.java.training.exercise3.regex;

public class SplitMethod {

	public static void main(String[] args) {

		String website = "https-www-google-com";
		String[] split = website.split("-");
		for (String str : split) {
			System.out.print(str + " ");
		}
	}
}