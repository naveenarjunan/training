/*Requirement: To split any random text using any pattern you desire.
 * 
 * Entity: SplitPattern
 * 
 * Function Declaration : public static void main(String[] args);
 * 
 * Jobs To be Done: 1. Get input from the user. 
 * 					2. Check if the input is null.
 * 						2.1. print the string cannot be null.
 * 					3. Create an array to add the split strings.
 * 					4. Get the split character as input.
 * 					5. Split the string and add it to the list.
 * 					6. Print the array of split strings to the output.
 *
 *PseudoCode:
 *
public class SplitPattern {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		String sentence = scanner.nextLine();
		if(sentence == null) {
			System.out.println("String is null");
		}

		String splitChar = scanner.next();
		Pattern pattern = Pattern.compile(splitChar);
		String[] list = pattern.split(sentence);

		for(String iter: list) {
			System.out.println(iter);
		}
		scanner.close();
	}

}

 * 
 */
package com.java.training.exercise3.regex;

import java.util.Scanner;
import java.util.regex.Pattern;

public class SplitPattern {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		String sentence = scanner.nextLine();
		if(sentence == null) {
			System.out.println("String is null");
		}

		String splitChar = scanner.next();
		Pattern pattern = Pattern.compile(splitChar);
		String[] list = pattern.split(sentence);

		for(String iter: list) {
			System.out.println(iter);
		}
		scanner.close();
	}

}