/*
Requirement:
  - Write a program which implement multithread using sleep() method.
Entity:
  - SleepThread
  - thread
  - MainDemo
Function Declaration:
  - public void run()
  - public static void main(String[]args)
Jobs to be done:
  1.Create a class SleepThread which extends thread 
  2.Create a method run() 
  3.Check the condition i < 5 
    3.1)Print multithread using sleep() method.
  4.If any thread has interrupted the current thread.
    4.1)throw exception that interrupted status of the current thread is cleared   
  5.sleep() is used to sleep a thread for the specified amount of time.  
  6.In main class create a two thread and pass the objects
  7.Start the thread process.
  
Pseudo Code:
public class SleepThread {
 public void run {
     for(int i=1;i<5;i++) {    
         try {  
             //call sleep method of thread
             Thread.sleep(1000);  
         } catch(InterruptedException e) {
         	System.out.println(e);
         }    
         //print current thread instance with loop variable
         System.out.println(Thread.currentThread().getName() + "   : " + i);    
     } 
 }
}
public class MainDemo {
 public static void main(String args[]) {        
   //create two thread instances
     SleepThread thread1 = new SleepThread();    
     SleepThread thread2 = new SleepThread();     
   //start threads one by one
     thread1.start();    
     thread2.start();      
 }   
}  
           
*/

package com.java.training.exercise3.threading;

class SleepThread extends Thread {    

	public void run() { 

		for(int i = 1 ;i < 5 ;i++) {    
			try {  
				//call sleep method of thread
				Thread.sleep(1000);  

			} catch(InterruptedException e) {
				System.out.println(e);
			}    
			//print current thread instance with loop variable
			System.out.println(Thread.currentThread().getName() + "   : " + i);    
		} 
	}   

}
public class MainDemo {

	public static void main(String[] args) {  

		//create two thread instances
		SleepThread thread1 = new SleepThread();    
		SleepThread thread2 = new SleepThread();    

		//start threads one by one
		thread1.start();    
		thread2.start();    

	}   

}  
