/*
Requirement:
	Convert this following code into a simple lambda expression
        int getValue(){
            return 5;
        }

Entity:
	SimpleExpression
	ConvertCode

Function Declaration:
	public int getValue();
	public static void main(String[] args) {}

Jobs To Be Done:
	1.Create an Interface named SimpleExpression.
		1.1)declare the method getvalue() inside the interface.
	2.Create lambda expression and return the value 5.
	3.print the value by invoking getValue() method.

Pseudo Code:
	interface SimpleExpression {

		//Declare the method getValue();
	}

	public class ConvertCode {

		public static void main(String[] args) {

			//Create Lambda Expression and return value
			System.out.println(simple.getValue());
		}
	}
 */

package com.java.training.exercise3.lambdaexpressions;

interface SimpleExpression {

	public int getValue();
}

public class ConvertCode {

	public static void main(String[] args) {

		SimpleExpression simple = (() -> {
			return 5;
		});

		System.out.println(simple.getValue());
	}
}