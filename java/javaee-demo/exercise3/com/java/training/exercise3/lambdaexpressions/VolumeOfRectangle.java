/* Requirement:
 * 		to print the volume of  a Rectangle using lambda expression.
 * 
   Entity:
   		Volume 
   Function declaration:
   		public static void main(String[] args)
	    public int volume(int length,int breath,int height)
	    
   Jobs to be done:
        1.An interface is created named LamdaInterfaceDifference
        2.Inside the interface , single method is declared named volume
        3.Inside the main method , create lambda expression 
        with three parameters which returns volume of rectangle.
 	    4.Print the volume of the rectangle.
 	    
 * Pseudo code:
  interface LambdaRectangle {
		public int volume(int length,int breath,int height);
  }

  public class Volume {

	public static void main(String[] args) {
			
			//lambda expression which returns volume.
			System.out.print("volume of rectangle:");
			System.out.println(lambdaRectangle.volume(4,6,8));
			
    }
}

 
 */
package com.java.training.exercise3.lambdaexpressions;

interface LambdaRectangle {

	public int volume(int length,int breath,int height);
}

public class VolumeOfRectangle {

	public static void main(String[] args) {

		LambdaRectangle lambdaRectangle = (length , breath , height) -> 
		length * breath * height ;

		System.out.print("volume of rectangle:");
		System.out.println(lambdaRectangle.volume(4,6,8));

	}
}