package com.java.training.exercise3.lambdaexpressions;
/*
To Find what is wrong with the following interface? and fix it.
    (int x, y) -> x + y;

Solution:
    In the given expression, The data type identifier must be given to both variables otherwise 
    both variables doesn't have data type identifier

Correct Expression:
    (int x, int y) -> x + y; or (x, y) -> x + y;
*/
