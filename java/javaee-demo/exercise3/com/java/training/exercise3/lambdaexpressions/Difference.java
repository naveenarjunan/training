/* Requirement:
 *     write a program to print difference of two numbers using lambda expression 
 *     and the single method interface.
 *     
 * Entity:
 *     LambdaInterfaceDifference
 *     Difference
 *     
 * Function Declaration:
 *     public static void main(String[] args)
 *     public int difference(int number1 , int number2);
 *     
 * Jobs To Be Done:
 * 	   1)create interface method named difference with two parameters.
 *     2)pass the parameters to difference method of the interface.
 *     3)returns the difference between two inputs.
 *     4)print the difference between two inputs.
 *     
 *pseudo code:
interface LambdaInterfaceDifference {

	public int difference(int number1 , int number2);
}

public class Difference {

	static LambdaInterfaceDifference lambdaInterfaceDifference = (number1 , number2) -> { 
		return number1 - number2 ;
	} ;

	public static void main(String[] args) {		
		System.out.println(lambdaInterfaceDifference.difference(12,3));
	}
}
 */


package com.java.training.exercise3.lambdaexpressions;

interface LambdaInterfaceDifference {

	public int difference(int number1 , int number2);
}

public class Difference {

	static LambdaInterfaceDifference lambdaInterfaceDifference = (number1 , number2) -> { 
		return number1 - number2 ;
	} ;

	public static void main(String[] args) {		
		System.out.println(lambdaInterfaceDifference.difference(12,3));
	}
}