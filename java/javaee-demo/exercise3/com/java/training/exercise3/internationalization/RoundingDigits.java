/*
 * Requirement:
 *      Write a code to rounding the value to maximum of 3 digits by setting maximum and minimum Fraction digits.
 *      
 * Entity:
 *      RoundingDigits
 *      
 * Function Signature:
 *     public static void main (String[] args)
 *     
 * Jobs to be Done:
 *      1)Create an object to access the particular locale.
 *      2)Set the Minimum and maximum Fraction Digits .
 *      3)Format the number to the specific locale and store it on number .
 *      4)print the number.
 *     
 * Pseudo code:
 * 
 * class RoundingDigits {
    public static void main (String[] args) {
        //Get the number format for specific locale
        rounding.setRoundingMode(RoundingMode.HALF_DOWN);
        rounding.setMinimumFractionDigits(1);
        rounding.setMaximumFractionDigits(3);
        String number1 = rounding.format(256.3654);
        System.out.println("After Rounding the values :" + number1);
    }

}
 * 
 *      
 */
package com.java.training.exercise3.internationalization;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Locale;

public class RoundingDigits {

	public static void main (String[] args) {

		NumberFormat rounding = NumberFormat.getInstance(new Locale("da", "DK"));
		rounding.setRoundingMode(RoundingMode.HALF_DOWN);
		rounding.setMinimumFractionDigits(1);
		rounding.setMaximumFractionDigits(3);
		String number1 = rounding.format(256.3654);
		System.out.println("After Rounding the values :" + number1);
	}

}
