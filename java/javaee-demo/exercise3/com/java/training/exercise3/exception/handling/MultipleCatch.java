/*Requirement:
 *    Difference between catching multiple exceptions and Mutiple catch blocks.
 *
 *Entity:
 *    DifferenceMultipleCatch
 *
 *Function Declaration:
 *    public static void main(String[] args)
 *
 *Jobs To Be Done:
 *    1)An array is created and 5 numbers are stored in it.
 *    2)For each element,
 *      2.1)Two is divided by each element in the array
 *      2.2)If it throws ArrayIndexOutOfBoundException it will print as Array size out of range.
 *      2.3)If it throws an ArthmeticException it will print as divide by 0 exception.
 *      2.4)Throws a exception it will print as exception handled.
 *      
 *Pseudo code:
 public class DifferenceMultipleCatch {
 
        public static void main(String[] args) {
 		
        int[] array = new int[] {1,2,3,0,5};
        
 		for(int i = 0; i < 6; i++) {
 			
 		    try {
 			    System.out.println(2/array[i]);
 			}
 		    catch(ArrayIndexOutOfBoundsException e) {
 				System.out.println("Array size out of range");
 		    }
 		    
 		    catch(ArithmeticException e) {
 				System.out.println("divided by 0 exception");
 		    }
 		    
 		    catch(Exception e) {
 				System.out.println("Exception handled");
 		    }
 		} 
 	}
 
 */

package com.java.training.exercise3.exception.handling;

public class MultipleCatch {

	public static void main(String[] args) {

		int[] array = new int[] {1,2,3,0,5};

		for(int i = 0; i < 6; i++) {

			try {
				System.out.println(2/array[i]);
			}
			catch(ArrayIndexOutOfBoundsException e) {
				System.out.println("Array size out of range");
			}

			catch(ArithmeticException e) {
				System.out.println("divided by 0 exception");
			}

			catch(Exception e) {
				System.out.println("Exception handled");
			}
		} 
	}
}

/*Difference:In Multiple catch blocks, try block will throw one exception and have several catch 
 *           block to handle unknown exception that is thrown. 
 * 		     In multiple exception,  single try block will throw several exception which has to
 *           be caught.
 */