package com.java.training.exercise3.exception.handling;

/*Requirement: only try block without catch and finally blocks
 * 
 *Entity: TryWithoutCatch
 *
 *Function declaration: public static void main(String[] args)
 *
 *Jobs To Be Done:1)In the try block
 *                  1.1)Print the result of two divided by zero.
 *Reason:In a a program try block should be accompanied either by catch block or finally block or
 *       both.
 * 		 A program without catch and finally block will throw syntax error as the exception will 
 *       not get handled.
 * 


package exception.handling;

public class TryWithoutCatch {
public static void main(String[] args) {
		
		try{
		   System.out.println(2/0);
		}
		//Exception in thread "main" java.lang.Error: Unresolved compilation problem: 
		//Syntax error, insert "Finally" to complete BlockStatements

	}

}*/