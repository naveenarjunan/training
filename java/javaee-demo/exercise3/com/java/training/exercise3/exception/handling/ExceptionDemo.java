/*Requirement:Handle and give the reason for the exception in the following code: 
              PROGRAM:
             public class Exception {  
             	public static void main(String[] args) {
  	                int arr[] ={1,2,3,4,5};
	                System.out.println(arr[7]);
                }
             }
         // Display the output.
 * 
 *Entity:
 *		Exception
 * 
 *Function Declaration:
 *		public static void main(String[] args)
 *
 *Jobs To Be Done:
 *		1)An int arr is created storing 5 elements.
 *      2)An element in the 7th index will be printed
 *           2.1)throws exception that Array index out of range.
 *                  
 *pseudo code:
  public class Exception {

        public static void main(String[] args) {
	       
        	int arr[] ={1,2,3,4,5};
        	
        	try {
                System.out.println(arr[7]);
        	} catch(ArrayIndexOutOfBoundsException e) {
				System.out.println("Array index out of range");
        	}
     }
}
*/

package com.java.training.exercise3.exception.handling;

public class ExceptionDemo {

	public static void main(String[] args) {

		int arr[] ={1,2,3,4,5};

		try {
			System.out.println(arr[7]);
		} catch(ArrayIndexOutOfBoundsException e) {
			System.out.println("Array index out of range");
		}
	}
}