/*Requirement:
 *    To Compare the checked and unchecked exception.
 *
 * Entity:
 *    CheckedUnchecked
 * 
 * Function Declaration:
 *    public static void main(String[] args)
 *    static void error()
 * 
 *Jobs To Be Done:
 *    1)Invoke the method error
 *      1.1)Throw a exception and print as No such method Found 
 *    2)Declare a variable named a and b as int and store 5 and 0 in it respectively.
 *    3)Print the result of a/b
 
*Pseudo code:
      public class CheckedUnchecked {
      
      	public static void main(String[] args) {
      		
      		try {
      		   error();
      		} catch(Exception e) {
      			//Exception handled
      		}
      		int a = 5;
      		int b = 0;
      	    System.out.println(a/b);
      	}
      
      }
 *
 */

package com.java.training.exercise3.exception.handling;

public class CheckedUnchecked {

	static void error() throws Exception {
		throw new Exception();
	}

	public static void main(String[] args) {

		try {

			//checked exception it must be handled otherwise rises exception in compile time
			error();
		} catch(Exception e) {
			System.out.println("No such method  found"); 
		}
		int a = 5;
		int b = 0;
		System.out.println(a/b);//shows error during runtime not in compile time
	}

}


/* *Answer:
 *Checked Exception:These are the exception thrown during the compile time but it can handled.
 *                  Here the method error will throw NO Such Method Found during compile time
 *                  so that it is handled.
 *Unchecked Exception:These are run time exception.Here b stores Zero and a/b is performed it will 
 *                    be compiled but it will a Arithmetic exception during the run time.
*/