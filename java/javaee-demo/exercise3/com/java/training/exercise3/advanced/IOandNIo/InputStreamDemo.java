/*
 * Requirements:
 *     To read a file using InputStream.
 *    
 * Entities:
 *     InputStreamDemo
 *     
 * Method Signature:
 *     public static void main(String[] args);
 *     
 * Jobs To Done:
 *     1.Create a reference for FileInputStream with file as constructor argument.
 *     2.Till the end of the file
 *         2.1)Read the content of the file.
 *         2.2)Print the content of the file.
 *     3.Close the created input stream.
 *
 * Pseudo code:
 *     class InputStreamDemo {
 *
 *         public static void main(String[] args) {
 *             InputStream inputstream = new FileInputStream("destination.txt");
 *             int data = inputstream.read();
 *             //read and print the content of the file.
 *             inputStream.close();
 *         }
 *     }
 */
 
package com.java.training.exercise3.advanced.IOandNIo;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class InputStreamDemo {

	public static void main(String[] args) throws IOException {

		InputStream inputStream = new FileInputStream("destination.txt");
		int data = inputStream.read();
		while(data != -1) {
			System.out.print((char) data);
			data = inputStream.read();
		}
		inputStream.close();
	}
}