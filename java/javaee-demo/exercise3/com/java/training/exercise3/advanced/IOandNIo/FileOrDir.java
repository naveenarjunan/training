/*
 * Requirements:
 *     To check the given path is file or directory.
 *    
 * Entities:
 *     FileOrDir
 *     
 * Method Signature:
 *     public static void main(String[] args);
 *     
 * Jobs To Done:
 *     1.Get the file or directory path.
 *     2.Check whether the given path is file or directory.
 *     3.if it is file 
 *         3.1)Print this is file
 *
 * Pseudo code:
 *     class FileOrDir {
 *
 *         public static void main(String[] args) {
 *             File file = new File("C:\\dbms\\training\\java\\javaee-demo");
 *	           if (file.isFile()) {
 *	        	   System.out.println("This is file");
 *	           } else {
 *	        	   System.out.println("This is directory");
 *	           }  
 *         }
 *     }   
 */

package com.java.training.exercise3.advanced.IOandNIo;

import java.io.File;

public class FileOrDir {

	public static void main(String[] args) {
		
		File file = new File("C:\\dbms\\training\\java\\javaee-demo");
		if (file.isFile()) {
			
			System.out.println("This is file");
			
		} else {
			
			System.out.println("This is directory");
			
		}
	}
}
