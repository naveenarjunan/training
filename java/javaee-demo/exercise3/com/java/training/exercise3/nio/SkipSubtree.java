/*
Requirement:
	Search any file using walkFileTree() method with enum instance(SKIP_SIBLINGS,SKIP_SUBTREE).

Entity:
	SkipSubtree
	
Function Declaration:
	public static void main(String[] args) {}
	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {}
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {}
	public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {}
	public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {}

Jobs To Be Done:
	1) Create a reference for Path as path and get the path of a file.
	2) Invoke walkFileTree() method and implement FileVisitor methods
		2.1) Invoke preVisitDirectory() method and visit the directory before entry with an instance SKIP_SUBTREE.
		2.2) Invoke visitFile() method and visit the file with an instance CONTINUE.
		2.3) Invoke visitFileFailed() method if traversal fails with an instance CONTINUE.
		2.1) Invoke postVisitDirectory() method and visit the directory after entry with an instance CONTINUE.
	3) Print the results.
	
Pseudo code:
public class SkipSubtree {
	
	public static void main(String[] args) throws IOException {
		
		Path path = Paths.get("D:\\Filevisitor\\dir");
		Files.walkFileTree(path, new FileVisitor<Path>() {
			  
			public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
				  
				System.out.println("pre visit dir:" + dir);
				return FileVisitResult.SKIP_SUBTREE;
			}

			  
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				  
			    System.out.println("visit file: " + file);
				return FileVisitResult.CONTINUE;
			}

			  
			public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
				  
			    System.out.println("visit file failed: " + file);
				return FileVisitResult.CONTINUE;
			}

			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				  
			    System.out.println("post visit directory: " + dir);
				return FileVisitResult.CONTINUE;
			}
		});
	}
}
*/

package com.java.training.exercise3.nio;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;

public class SkipSubtree {

	public static void main(String[] args) throws IOException {

		Path path = Paths.get("D:\\Filevisitor\\dir");
		Files.walkFileTree(path, new FileVisitor<Path>() {

			public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {

				System.out.println("pre visit dir:" + dir);
				return FileVisitResult.SKIP_SUBTREE;
			}


			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

				System.out.println("visit file: " + file);
				return FileVisitResult.CONTINUE;
			}


			public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {

				System.out.println("visit file failed: " + file);
				return FileVisitResult.CONTINUE;
			}

			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {

				System.out.println("post visit directory: " + dir);
				return FileVisitResult.CONTINUE;
			}
		});
	}
}
