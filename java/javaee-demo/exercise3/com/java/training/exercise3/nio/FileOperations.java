/*
Requirement:
	Perform file operations using exists(), createDirectory(), copy(), move() and delete() methods.

Entity:
	FileOperations
	
Function Declaration:
	public static void main(String[] args) {}

Jobs To Be Done:
	1) Create a reference for Path as path and get the path.
	2) Invoke createDirectory() method and create new directory.
	3) Create a reference for Path as sourceFile and get the path of source.txt file.
	4) invoke createFile() method and create new file.
	5) Check if the file exists or not
		5.1) if file exists print "File Exists"
		5.2) else print "File does not Exists"
	6) Invoke copy() method and copy the file.
	7) Invoke move() method and move the file.
	8) Invoke delete() method and delete the file.
	
Pseudo code:
public class FileOperations {

	@SuppressWarnings("unused")
	public static void main(String[] args) throws IOException {

		Path path = Paths.get("C:\\dbms\\training\\java\\javaee-demo\\exercise3\\com\\java\\training\\exercise3\\nio\\file");
		Path newDirectory = Files.createDirectory(path);
		Path sourceFile = Paths.get("C:\\dbms\\training\\java\\javaee-demo\\exercise3\\com\\java\\training\\exercise3\\nio\\file\\source.txt");
		Path createdFilePath = Files.createFile(sourceFile); 

		boolean pathExists = Files.exists(sourceFile);

		if(pathExists) {

			System.out.println("File exists ");
		} 
		else {

			System.out.println("File dos not exists ");
		}

		Path path1 = Paths.get("C:\\dbms\\training\\java\\javaee-demo\\exercise3\\com\\java\\training\\exercise3\\nio\\file1");
		Path newDirectory1 = Files.createDirectory(path1);

		Path destinationFile = Paths.get("C:\\dbms\\training\\java\\javaee-demo\\exercise3\\com\\java\\training\\exercise3\\nio\\file1\\destination.txt");
		Files.copy(sourceFile,destinationFile);

		Path moveFile = Paths.get("C:\\dbms\\training\\java\\javaee-demo\\exercise3\\com\\java\\training\\exercise3\\nio\\file\\source.txt");

		Files.move(sourceFile, moveFile);
		Files.delete(moveFile);                                                           
		Files.delete(destinationFile);
	}
}

*/

package com.java.training.exercise3.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileOperations {

	@SuppressWarnings("unused")
	public static void main(String[] args) throws IOException {

		Path path = Paths.get("C:\\dbms\\training\\java\\javaee-demo\\exercise3\\com\\java\\training\\exercise3\\nio\\file");
		Path newDirectory = Files.createDirectory(path);
		Path sourceFile = Paths.get("C:\\dbms\\training\\java\\javaee-demo\\exercise3\\com\\java\\training\\exercise3\\nio\\file\\source.txt");
		Path createdFilePath = Files.createFile(sourceFile); 

		boolean pathExists = Files.exists(sourceFile);

		if(pathExists) {

			System.out.println("File exists ");
		} 
		else {

			System.out.println("File dos not exists ");
		}

		Path path1 = Paths.get("C:\\dbms\\training\\java\\javaee-demo\\exercise3\\com\\java\\training\\exercise3\\nio\\file1");
		Path newDirectory1 = Files.createDirectory(path1);

		Path destinationFile = Paths.get("C:\\dbms\\training\\java\\javaee-demo\\exercise3\\com\\java\\training\\exercise3\\nio\\file1\\destination.txt");
		Files.copy(sourceFile,destinationFile);

		Path moveFile = Paths.get("C:\\dbms\\training\\java\\javaee-demo\\exercise3\\com\\java\\training\\exercise3\\nio\\file\\source.txt");

		Files.move(sourceFile, moveFile);
		Files.delete(moveFile);                                                           
		Files.delete(destinationFile);
	}
}
