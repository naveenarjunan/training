/*
 * Requirement:
 *     Perform file operations using delete() method.
 *     
 * Entity:
 *     DeleteMethod
 *     
 * Function Signature:
 *     public static void main(String[] args)
 *     
 * Jobs to be done;
 *     1) Create path instance for a file which has to be delete.
 *     2) Delete the file using delete() method.
 *     3) Print file has been deleted successfully.
 * Pseudocode:
 * 
 * class DeleteMethod {

        public static void main(String[] args) throws IOException {
            //Create path instance for a file which has to be delete and name it as sourcefile.
             Files.delete(sourceFile);
            System.out.println("File has been deleted successfully");
       }
  }
 */
package com.java.training.exercise3.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DeleteMethod {

    public static void main(String[] args) throws IOException {

        Path sourceFile = Paths.get("C:\\1dev\\training\\exampletext.txt");
        Files.delete(sourceFile);
        System.out.println("File has been deleted successfully");
    }

}
