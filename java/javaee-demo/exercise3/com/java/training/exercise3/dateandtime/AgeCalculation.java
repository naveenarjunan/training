/*
Requirement:
    To write a java code to print my age
    
Entity:
    AgeCalculation
    
Function declaration:
    public static void man(String[] args)
    
Jobs to be done:
    1. Create two object instances of LocalDate,
    	1.1)first dateOfBirth to store your date of birth.
        1.2)and then age to store current date.
    2. Create an instance for Period as myAge and calculate difference between the dateOfBirth and
       age that is stored in myAge using between() method
    3. Print the age.
    
Pseudo code:
class AgeCalculation {
    
    public static void main(String[] args) {
        LocalDate dateOfBirth = LocalDate.of(2001, 1, 11);
        LocalDate age = LocalDate.now();
        Period myAge = Period.between(dateOfBirth, age);
        System.out.println("I am " + myAge.getYears() + " " + "years old");
    }
}
*/

package com.java.training.exercise3.dateandtime;

import java.time.LocalDate;
import java.time.Period;

public class AgeCalculation {
    
    public static void main(String[] args) {
    	
        LocalDate dateOfBirth = LocalDate.of(2001, 3, 19);
        LocalDate age = LocalDate.now();
        Period myAge = Period.between(dateOfBirth, age);
        System.out.println("I am " + myAge.getYears() + " " + "years old");
    }
}
