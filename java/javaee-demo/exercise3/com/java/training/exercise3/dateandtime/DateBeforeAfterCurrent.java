/*
Requirement:
    To write a Java program to get the dates 1 year before and after today.
      
Entity:
    LocalDateDemo
    
Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1. An instance is created for Calendar as calendar that gets the calendar using current time zone.
        1.1)using the method getTime() the current date is printed.
    2. By decreasing a year from the current year, print the date before 1 year.
    3. By adding a year from the current year, print the date after year. 
    
Pseudo code:
class BeforeAndAfterYearDemo {
    
    public static void main(String[] args) {
        Calendar calender = Calendar.getInstance();
        System.out.println("The current date is: " + " " + calender.getTime());
        
        calender.add(Calendar.YEAR, -1);
        System.out.println("Date before 1 year is: " + " " + calender.getTime());
        
        calender.add(Calendar.YEAR, 2);
        System.out.println("The day 1 year after the current day is: " + " " + calender.getTime());
    }
}


*/

package com.java.training.exercise3.dateandtime;

import java.util.Calendar;

public class DateBeforeAfterCurrent {
    
    public static void main(String[] args) {
    	
        Calendar calender = Calendar.getInstance();
        System.out.println("The current date is: " + " " + calender.getTime());
        
        calender.add(Calendar.YEAR, -1);
        System.out.println("Date before 1 year is: " + " " + calender.getTime());
        
        calender.add(Calendar.YEAR, 2);
        System.out.println("The day 1 year after the current day is: " + " " + calender.getTime());
    }
}
