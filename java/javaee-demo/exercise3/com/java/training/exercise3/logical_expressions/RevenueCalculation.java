/*
 * Requirement: 
 *     Write a Java program to calculate the revenue from a sale based on the unit price and quantity of a product input by the user.
 * The discount rate is 10% for the quantity purchased between 100 and 120 units, and 15% for the quantity purchased greater than 120 units. If the quantity purchased is less than 100 units, the discount rate is 0%. See the example output as shown below:
 * Enter unit price: 25
 * Enter quantity: 110
 * The revenue from sale: 2475.0$
 * After discount: 275.0$(10.0%)
 * 
 * Entities:
 *    CalculateRevenue
 *    
 * Function Signature:
 *    public static void main(String[] args)
 * 
 * Jobs to be done:
 *    1) Create a float variable named revenue and store 0 in it.
 *    2) Create a float variable named discount_rate and store 0 in it.
 *    3) Create a float variable named discount_amount and store 0 in it.
 *    4) Create an object for Scanner named scanner. 
 *    5) Get the unit price as input as input from the user and store it in unitprice.
 *    6) Get the quantity as input as input from the user and store it in quantity.
 *    4) check whether the quantity is less than 100
 *        4.1) if it is less than 100,store unit price *quantity in revenue.
 *        4.2) if it is not less than 100 then check whether the quantity is greater than 100 and
 *             less than or equal to 120
 *             4.2.1) if it is true, set discount rate as 10/100 and store revenue value as 
 *                  unitprice * quantity and  the discount_amount as revenue * discount_rate and the
 *                  revenue as revenue - discount_amount.   
 *             4.2.2) Check whether the - is greater than 120.
 *                 4.2.2.1)if it is true, set discount rate as 15/100 and sstore revenue value as 
 *                  unitprice * quantity and  the discount_amount as revenue * discount_rate and the
 *                  revenue as revenue - discount_amount.   
 *    5)Print revenue and discount amount.
 * pseudo code:
 * 
  class CalculateRevenue {
    
    public static void main(String[] args) {
        float revenue = 0f;
        float discount_rate = 0f;
        float discount_amount = 0f;

        Scanner scanner = new Scanner(System.in);
        float unitprice = scanner.nextFloat();
        int quantity = scanner.nextInt();
        if (quantity < 100)
            revenue = unitprice * quantity;
        else if (quantity >= 100 && quantity <= 120) {
            discount_rate = (float) 10 / 100;
            revenue = unitprice * quantity;
            discount_amount = revenue * discount_rate;
            revenue -= discount_amount;
        }

        else if (quantity > 120) {
            discount_rate = (float) 15 / 100;
            revenue = unitprice * quantity;
            discount_amount = revenue * discount_rate;
            revenue -= discount_amount;
        }
        System.out.println("The revenue from sale:" + revenue + "$");
        System.out.println("After discount:" + discount_amount + "$(" + discount_rate * 100 + "%)");

    }

}

 */
package com.java.training.exercise3.logical_expressions;

import java.util.Scanner;

public class RevenueCalculation {

	public static void main(String[] args) {
		
		float revenue = 0f;
		float discount_rate = 0f;
		float discount_amount = 0f;

		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter unit price:");
		float unitprice = scanner.nextFloat();
		System.out.print("Enter quantity:");
		int quantity = scanner.nextInt();

		if (quantity < 100) {
			revenue = unitprice * quantity;
		}	
		else if (quantity >= 100 && quantity <= 120) {
			discount_rate = (float) 10 / 100;
			revenue = unitprice * quantity;
			discount_amount = revenue * discount_rate;
			revenue -= discount_amount;
		}

		else if (quantity > 120) {
			discount_rate = (float) 15 / 100;
			revenue = unitprice * quantity;
			discount_amount = revenue * discount_rate;
			revenue -= discount_amount;
		}

		System.out.println("The revenue from sale:" + revenue + "$");
		System.out.println("After discount:" + discount_amount + "$(" + discount_rate * 100 + "%)");
		scanner.close();
	}
}
