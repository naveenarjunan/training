/*
 * Requirement:
 *    write a program to display the following using escape sequence in java
 *     A. My favorite book is "Twilight" by Stephanie Meyer
 *     B. She walks in beauty, like the night, 
 *        Of cloudless climes and starry skies 
 *        And all that's best of dark and bright 
 *        Meet in her aspect and her eyes...
 *     C. "Escaping characters", � 2019 Java
 * Entity:
 *    EscapeSequence
 * Function Signature:
 *    public static void main(String[] args)
 * Jobs to be done:
 *   1) Print the given statements by using escape sequence.
 * Pseudo code:
 * 
 * public class EscapeSequence {

    public static void main(String[] args) {

        System.out.println("My favorite book is \"Twilight\" by Stephanie Meyer");

        System.out.println(
                "She walks in beauty, like the night, \nOf cloudless climes and starry skies\nAnd all that's best of dark and bright\nMeet in her aspect and her eyes...");

        System.out.println("\"Escaping characters\", \u00A9 2020 Java");

    }
}

 * 
 */
package com.java.training.exercise3.logical_expressions;

public class EscapeSequence {

	public static void main(String[] args) {

		System.out.println("My favorite book is \"Twilight\" by Stephanie Meyer");

		System.out.println(
				"She walks in beauty, like the night, \nOf cloudless climes and starry skies\nAnd all that's best of dark and bright\nMeet in her aspect and her eyes...");

		System.out.println("\"Escaping characters\", \u00A9 2020 Java");

	}
}
