/*Requirement: 
 * 		Write a program for ByteArrayInputStream class to read byte array as input stream.
 * 
 * Entity: 
 * 		ReadByteArrayAsInput
 * 
 * Method Signature: 
 * 		public static void main(String[] args);
 * 
 * Jobs to be done:
 * 		 1. Create a byte array named byteArray and store values to it.
 *   	 2. Create a ByteArrayInputStream by passing byteArray as argument.
 * 	     3. While the byte array is not empty
 * 			3.1) Convert the int to char 
 *          3.2) Print the buffer element and the character.
 *          
 * PseudoCode:
 
public class ReadByteArrayAsInput {
	
	public static void main(String[] args) throws IOException {  
		
	    byte[] byteArray = { 35, 36, 37, 38 };  
	    
	    
	    // Create the new byte array input stream  
	    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArray);  
	    int k ;  
	    while ((k = byteArrayInputStream.read()) != -1) {   
	    	
	    	//Conversion of a byte into character
	    	char ch = (char) k;  
	    	System.out.println("ASCII value of Character is:" + k + "; Special character is: " + ch);  
	    }  
	}  
}
 */
package com.java.training.exercise3.iostreams;

import java.io.ByteArrayInputStream;
import java.io.IOException;

public class ReadByteArrayAsInput {
	
	public static void main(String[] args) throws IOException {  
		
	    byte[] byteArray = { 35, 36, 37, 38 };  
	    
	    
	    // Create the new byte array input stream  
	    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArray);  
	    int k ;  
	    while ((k = byteArrayInputStream.read()) != -1) {   
	    	
	    	//Conversion of a byte into character
	    	char ch = (char) k;  
	    	System.out.println("ASCII value of Character is:" + k + "; Special character is: " + ch);  
	    }  
	}  
}