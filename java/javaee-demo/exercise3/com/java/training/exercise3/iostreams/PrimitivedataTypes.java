/*Requirement:
 * 		 program to write primitive datatypes to file using by dataoutputstream.
 * 
 * Entity :
 * 		 PrimitiveDataTypes
 * 
 * Method Signature:
 * 		 public static void main(String[] args);
 * 
 * Jobs to be done: 
 * 		1. Declare the FileOutputStream to save the destination file for writing.
 * 		2. Declare the DatOutputStream using declared FileOutputStream.
 * 		3. Write the primitive data types to file.
 * 		4. Close the DataOutputStream.
 * 
 * PseudoCode:
public class PrimitiveDataTypes {
	 
	public static void main(String[] args) throws IOException {
	           
		FileOutputStream fileOutputStream = new FileOutputStream("C:\\1dev\\training\\java\\javaee-demo\\primitivetype.txt");
	    DataOutputStream dataOutputStream = new DataOutputStream(fileOutputStream);
	    dataOutputStream.writeInt(50000);
	    dataOutputStream.writeShort(2500);
	    dataOutputStream.writeByte(120);
	    dataOutputStream.close();     
	}
}

 */
package com.java.training.exercise3.iostreams;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class PrimitivedataTypes {
	 
	public static void main(String[] args) throws IOException {
	           
		FileOutputStream fileOutputStream = new FileOutputStream("C:\\dbms\\training\\java\\javaee-demo\\primitivetype.txt");
	    DataOutputStream dataOutputStream = new DataOutputStream(fileOutputStream);
	    dataOutputStream.writeInt(50000);
	    dataOutputStream.writeShort(2500);
	    dataOutputStream.writeByte(120);
	    dataOutputStream.close();     
	}
}