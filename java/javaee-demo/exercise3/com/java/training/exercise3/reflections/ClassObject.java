package com.java.training.exercise3.reflections;

/*Requirement:
	1. What is a Class object. How do you get a Class object via reflection?
		
		 Solution:
		 Class object:The entry point for all reflection operations is java.lang.Class. With the exception
		              of java.lang.reflect.ReflectPermission .
		              If an instance of an object is available, then the simplest way to get its Class is
		              to invoke Object.getClass().
		               The getClass() method is used to get the name of the class to which an object belongs.
 *   
 *   2.How do you access the parent class of a class?
		 Solution:
		 Access parent class :
		 		Class superclass = aClass.getSuperclass();
		                      Here aClass is a parent class
*/