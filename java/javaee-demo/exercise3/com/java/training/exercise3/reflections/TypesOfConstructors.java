package com.java.training.exercise3.reflections;

/* - Explain the types of constructors with an example.

In Java, constructors are of 3 types:

  1.  No-Arg Constructor
  2.  Default Constructor
  3.  Parameterized Constructor
  
  
 1. No-Arg Constructor :
       A Java constructor may or may not have any parameters (arguments). 
	   If a constructor does not accept any parameters, it is known as a no-arg constructor. 
	   
2.  Default Constructor :
       If you do not create any constructors, the Java compiler will automatically create a no-argument constructor during run-time.
	   This constructor is known as the default constructor. 
	   The default constructor initializes any uninitialized instance variables with default values.

3.  Parameterized Constructor :
       Similar to methods, we can pass parameters to a constructor.
	   Such constructors are known as a parameterized constructor. 	   
*/