/*
Requirement:
    To create a private constructor with main function.
    
Entity:
    PrivateConstructors
    
Function declaration:
	private PrivateConstructors()
    public static void main(String[] args)
    
Jobs to be done:
    1. Create a private method of its class name.
    2. Create an instance of the current class.
    
Pseudo code:
class PrivateConstructors {

    // create private constructor
    private PrivateConstructor() {
        System.out.println(message);
    }

    public static void main(String[] args) {

        PrivateConstructor privateConstructor = new PrivateConstructor();

    }
}

*/
package com.java.training.exercise3.reflections;

public class PrivateConstructors {

	// create private constructor
	private PrivateConstructors() {
		System.out.println("This is a private constructor.");
	}

	public static void main(String[] args) {

		@SuppressWarnings("unused")
		PrivateConstructors privateConstructor = new PrivateConstructors();

	}
}
