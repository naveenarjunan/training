/*Requirement:+ Create a list
              => Add 10 values in the list
              => Create another list and perform addAll() method with it
              => Find the index of some value with indexOf() and lastIndexOf()
              => Print the values in the list using 
        				- For loop
        				- For Each
        				- Iterator
        				- Stream API
			 => Convert the list to a set
    		 => Convert the list to a array
             + Explain about contains(), subList(), retainAll() and give example
             
 * Entities:
 * 		ListOperation
 * 
 * Function Declaration:
 * 		public static void main(String[] args)
 * 
 * Jobs To Be Done:
 * 1.consider the program
 * 2.Two Arraylist is created named list and list1
 * 3.To insert the elements add method is used and the add method is inside the for loop
 * 4.for each iteration ,in for loop,one element is added to the list
 * 5.The elements of list are added to list1 by using addAll method
 * 6.The first index of element is printed using indexOf method
 * 7.last index of the particular element is printed using latIndexOf method
 * 
 *                 
 *                 
 */

package com.java.training.exercise3.list;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.ListIterator;
import java.util.Set;
import java.util.stream.Stream;

public class ListOperation {

	public static void main(String[] args) {

		ArrayList<Integer> list = new ArrayList<Integer>();
		ArrayList<Integer> list1 = new ArrayList<Integer>();

		//inserting elements to the list
		for(int i = 0 ;i <= 10 ;i++) {
			list.add(i);
		}
		list.add(1);

		//using addall
		list1.addAll(list);

		//print the first index and last index
		System.out.println("print the first index of 1:" + list.indexOf(1));
		System.out.println("print the last index of 1:" + list.lastIndexOf(1));

		//print list using for loop
		System.out.println("printing list using for loop:");
		for(int number = 0 ;number <= 9 ;number++) {
			System.out.print(number);
		}
		System.out.println();

		//print list using for each loop
		System.out.println("printing list using for each loop:");
		for (Integer number : list) {
			System.out.print(number);
		}
		System.out.println();

		//printing list using for stream api 
		System.out.println("printing list using stream api");
		Stream<Integer> stream = list.stream();
		stream.forEach((number) -> System.out.print(number));
		System.out.println();

		//printing the list using iterator
		System.out.println("printing list using Iterator:");
		ListIterator<Integer> number=list.listIterator();
		while(number.hasNext())
		{
			System.out.print(number.next());
		}

		//convert list to set
		System.out.println();
		System.out.println("set");
		Set<Integer> set = new HashSet<>(); 
		set.addAll(list);
		System.out.println(set);

		//convert list to array
		System.out.println("Array");
		list.toArray();
		System.out.println(list);

		//Contains
		System.out.println("contains");
		if(list.contains(1)) {
			System.out.println("1 is present");
		} else {
			System.out.println("1 is not present");
		}

		//sublist
		System.out.println(list.subList(5, 9));

		//retainall
		ArrayList<Integer> list3 = new ArrayList<Integer>();
		list3.add(1);
		list3.add(10);
		list3.add(4);
		list3.add(11);
		System.out.println("retained numbers:");
		System.out.println(list3.retainAll(list1));       
	}

}