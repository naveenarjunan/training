/*Requirement:
 *   Write a program to add 5 elements to a xml file and print the elements in the xml file using list. 
 *   Remove the 3rd element from the xml file and print the xml file.
 *   
 * Entities:
 *     XmlProperties
 * Function Signature:
 * 
 *    public static void main(String[] args)
 *    
 * Jobs to be done:
 *   1) Create a object for properties.
 *   2) Setting some property as (key - value) pair in properties.
 *   3) Create a object for FileOutputStream and assign a name for xml file.
 *   4) Store the properties in FileOutputStream as xml file.
 *   5) Print the properties stored in xml file successfully
 *   6) Print the elements in xml file using list. 
 *   7) Remove the 3rd element and print the properties.
 *   
 *Pseudocode:
 *
 *class XmlProperties {
 *
      public static void main(String[] args) throws IOException {
        //Add the element to the properties.
        OutputStream output = new FileOutputStream("Language.xml");
        //store the property element to the Outputstream.
        System.out.println("Properties stored in xml file successfully");
        
        properties.list(System.out);

        properties.remove("3rd elemeent key);
        System.out.println("After removing the element from the xml file ");
        properties.list(System.out);
        }
  }   
        
 */
package com.java.training.exercise3.properties;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class XmlProperties {

	public static void main(String[] args) throws IOException {

		Properties properties = new Properties();
		properties.setProperty("America", "English");
		properties.setProperty("Germany", "German");
		properties.setProperty("Russia", "Russian");
		properties.setProperty("China", "Mandarin");

		OutputStream output = new FileOutputStream("Language.xml");
		properties.storeToXML(output, "Countries and Language.");
		System.out.println("Properties stored in xml file successfully");

		properties.list(System.out);

		properties.remove("Russia");
		System.out.println("After removing the element from the xml file ");
		properties.list(System.out);

	}

}
