/*
 * Requirements: 
 *     To write a program to demonstrate generics - class objects as type literals.
 *
 * Entities:
 *     Dog 
 *     Animals
 *    
 * Method Signature:
 *     public static <T> boolean checkInterface(Class<?> theClass);
 *     public void sound();
 *     public static void main(String[] args);
 *    
 * Jobs To Be Done:
 *     1.Define a method checkInterface which returns the boolean according to the class object passed as argument
 *     2.Define the method sound which is declared in the Animal interface.
 *     3.Create the references for the class Class with different types 
 *     4.Invoke the method checkInterface with various class references and print the returned boolean value
 *     5.Get the class name and type of the class references with getClass() and getName() methods respectively. 
 *     6.Create the class reference with forName() method and invoke the method checkInterface. 
 *     
 * Pseudo code:
 * 
 *     interface Animal {

      		public void sound();
       }
 *     class Dog implements Animal {
 * 
 *          public boolean checkInterface(Class<?> theClass) {
 *              return theClass.isInterface();
 *          }
 * 
 *          public void sound() {
 *              System.out.println("Barking");
 *          }
 * 
 *          public static void main(String[] args) {
 *              Class<Integer> intClass = int.class;            
 *              boolean boolean1 = checkInterface(intClass);
 *              System.out.println(boolean1);                   
 *              System.out.println(intClass.getClass());        
 *              System.out.println(intClass.getName());         
 *
 *              boolean boolean2 = checkInterface(Dog.class);
 *              System.out.println(boolean2);                   
 *              System.out.println(Dog.class.getClass());       
 *              System.out.println(Dog.class.getName());        
 *      
 *              boolean boolean3 = checkInterface(Animal.class);
 *              System.out.println(boolean3);                   
 *              System.out.println(Animal.class.getClass());    
 *              System.out.println(Animal.class.getName());     
 *      
 *              try {
 *                  Class<?> testClass = Class.forName("Dog");
 *                  System.out.println(testClass.getClass());
 *                  System.out.println(testClass.getName());
 *              } catch (ClassNotFoundException ex) {
 *                  System.out.println(ex.toString());
 *              }
 *          }
 *     }     
 */
package com.java.training.exercise3.generics;

interface Animal {

	public void sound();
}

public class Dog implements Animal {

	public static <T> boolean checkInterface(Class<?> theClass) {
		return theClass.isInterface();
	}

	public void sound() {
		System.out.println("Barking");
	}

	public static void main(String[] args) {
		
		Class<Integer> intClass = int.class;
		boolean boolean1 = checkInterface(intClass);
		System.out.println(boolean1); 
		System.out.println(intClass.getClass());
		System.out.println(intClass.getName()); 

		boolean boolean2 = checkInterface(Dog.class);
		System.out.println(boolean2); 
		System.out.println(Dog.class.getClass());
		System.out.println(Dog.class.getName()); 

		boolean boolean3 = checkInterface(Animal.class);
		System.out.println(boolean3); 
		System.out.println(Animal.class.getClass());
		System.out.println(Animal.class.getName()); 

		try {
			Class<?> testClass = Class.forName("Dog");
			System.out.println(testClass.getClass());
			System.out.println(testClass.getName());
		} catch (ClassNotFoundException ex) {
			System.out.println(ex.toString());
		}
	}
}