/*
 * Requirements : 
 *      Write a generic method to count the number of elements in a collection that have a specific
 * property (for example, odd integers, prime numbers, palindromes).
 *
 * Entities :
 *      CountSpecificProperty
 *      
 * Function Declaration :
 *      public static void main(String[] args)
 *      public static int count(ArrayList<Integer> list)
 *      
 * Jobs To Be Done:
 *      1)Create the count method which returns the count of odd numbers present in a list
 *      	1.1)using for loop, get the elements from the collection.
 *      	1.2)under if condition check the condition for odd integers.
 *      2)In the main method, create an Arraylist. 
 *      3)Add elements to the list.
 *      4)Call the count method and print the number of odd numbers.
 *      
 *pseudo code:
 
        public class CountSpecificProperty {
           
           public static int count(ArrayList<Integer> list) {
               int c = 0;
               for(int elements : list) 
               //if condition to check odd integers and return c.
       
           public static void main(String[] args) {
               ArrayList<Integer> list = new ArrayList<>();
               //add elements to the list
               System.out.println("Number of odd integers : " + count(list));
           }
         }
		}
 */

package com.java.training.exercise3.generics;

import java.util.ArrayList;

public class CountSpecificProperty {

	public static int count(ArrayList<Integer> list) {
		
		int c = 0;
		for(int elements : list) {
			if(elements % 2 != 0) {
				c++;
			}
		}
		return c;
	}

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(5);
		list.add(4);
		list.add(3);
		list.add(2);
		list.add(1);
		System.out.println("Number of odd integers : " + count(list));

	}

}
