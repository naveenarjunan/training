/*
Requirement :
    Write a generic method to exchange the positions of two different elements in an array.

Entity:
    Exchange

Function Declaration:
    public static void swap(Integer[] number, int number1, int number2)
    public static void main(String[] args) 

Jobs to be done:
    1) Create a method swap() of type T and swap the elements using temporary variables.
    2) Create method swap by using Collections.swap().
    3) Under a main method create an array and call the swap method.
    4) print the result.
    
Pseudo code:
	public class Exchange {
	
    	public static <T> void swap(T[] number, int number1, int number2) {
    	
        	//Swap the given two numbers
    	}

    	public static void main(String[] args) {
    	
        	Integer[] list = {110, 250, 300, 234, 540, 600};
        
        	//Print the list before swapping
        
        	swap(list, 2, 4);
        
        	//Print the list after swapping
    	}
	}
	
*/

package com.java.training.exercise3.generics;

import java.util.Arrays;

public class ExchangeElements {

	public static void swap(Integer[] number, int number1, int number2) {

		Integer temporary = number[number1];
		number[number1] = number[number2];
		number[number2] = temporary;
	}

	public static void main(String[] args) {

		Integer[] list = {10, 20, 30, 40, 50, 60};

		System.out.print("Before Swapping : ");
		System.out.print("\t" + Arrays.toString(list));

		swap(list, 2, 4);

		System.out.print("\nAfter Swapping : ");
		System.out.print("\t" + Arrays.toString(list));
	}
}