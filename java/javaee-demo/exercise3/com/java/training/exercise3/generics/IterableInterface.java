/* 
Requirement:
    To Write a program to print employees name list by implementing iterable interface.
    
Entity:
    IterableInterface
    
Function Declaration:
    public static void main(String[] args)
    
Jobs To Be Done:
    1.Create the MyIterable class that implements the Iterable.
    2.Create the local variable list as generic type.
    3.Create a method MyIterable of generic type  and assigning already defined String values from the main class.
    4.Create the main class as IterableInterface .
    5.Declare the String values in the employeeName variable.
    6.Create the object myList for the variables to be iterated.
    7.After assigning the variables in the list by iterator implement, the final result is printed.
    
Pseudo code:
 class MyIterable<A> implements Iterable<A> {
	 
    public List<A> list;
 
    public MyIterable(A [] a) {
 
        list = Arrays.asList(a);
    }
    
    public Iterator<A> iterator() {
 
        return list.iterator();
    }
}

public class IterableInterface {
	
	public static void main(String [] args) {
		 
        String[] employeeName ={"Anu", "Arun", "kavi", "abi", "karan"};
        
        MyIterable<String> myList = new MyIterable<>(employeeName);
        
        System.out.println("Employees Name :");
        for (String i : myList) {
            System.out.println(i);
        }
    }
}

*/

package com.java.training.exercise3.generics;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

class MyIterable<A> implements Iterable<A> {

	public List<A> list;

	public MyIterable(A [] a) {

		list = Arrays.asList(a);
	}

	public Iterator<A> iterator() {

		return list.iterator();
	}
}

public class IterableInterface {

	public static void main(String [] args) {

		String[] employeeName = {"Anu", "Arun", "kavi", "abi", "karan"};

		MyIterable<String> myList = new MyIterable<>(employeeName);

		System.out.println("Employees Name :");
		for (String i : myList) {
			System.out.println(i);
		}
	}
}
