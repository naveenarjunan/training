/* 
Requirement:
    To find the maximum element of the given program using generic method.

Entity:
    MaximumElement

Function Declaration:
    public static void main(String[] args)
    public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int begin, int end)
    
Jobs to be Done:
    1. Declare a method public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int begin, int end)
    2. Compare the elements in the list using for loop and if condition.
      2.1)if the compared element is bigger return the element or go for the next element
    3. Under the main method, 
    	3.1)create an object list of generic type
    	3.2)add the elements to the list.
    4. Call the method max() and print the result.
    
    
pseudo code:

	public class  MaximumElement {
    
    	public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int begin, int end) {
        	T maximumElement = list.get(begin);
        	for (++begin; begin < end; ++begin) {
            	if (maximumElement.compareTo(list.get(begin)) < 0) {
                	maximumElement = list.get(begin);
            	}
        	}   
        	return maximumElement;
    	}
    
    	public static void main(String[] args) {
    	
        	List<Integer> list = new LinkedList<>();
        	//add the elements to the list
        	System.out.println(max(list, 0, list.size()));
    	}
	}
*/

package com.java.training.exercise3.generics;

import java.util.LinkedList;
import java.util.List;

public class  MaximumElement {

	public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int begin, int end) {

		T maximumElement = list.get(begin);

		for (++begin; begin < end; ++begin) {

			if (maximumElement.compareTo(list.get(begin)) < 0) {

				maximumElement = list.get(begin);
			}
		}
		return maximumElement;
	}

	public static void main(String[] args) {

		List<Integer> list = new LinkedList<>();

		list.add(100);
		list.add(20);
		list.add(30);
		list.add(40);
		list.add(500);
		list.add(60);
		list.add(70);
		list.add(80);
		list.add(90);
		list.add(10);

		System.out.println(max(list, 0, list.size()));
	}
}