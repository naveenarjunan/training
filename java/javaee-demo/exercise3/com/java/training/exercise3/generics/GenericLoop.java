/*
Requirement :
    Write a program to demonstrate generic - for loop, for list, set and map

Entity:
    GenericLoop

Function Declaration:
    public static void main(String[] args );

Jobs To be Done :
    1.create the object set as generic type.
 *    1.1)Put values in the set, prints the set using for each loop.
 *  2.create the object list as generic type.
 *    1.1)Put values in the list, prints the list using for each loop.
    3.create the object map as generic type.
 *    3.1)Put key and values pairs in the map, prints the map using for each loop.

pseudo code:

public class GenericLoop {

    public static void main(String[] args) {

        //set using for each loop
        Set<String> set = new HashSet<>();
        //add elements to the set
        //iterate using for loop

        //list using for each loop
        List<Integer> list = new ArrayList<>();
        //add elements to the list
        //iterate using for loop

        //map using for each loop
        HashMap<String , String> map = new HashMap<>();
        //add key and value pair to the map
        //iterate using for loop
    }
}
 */


package com.java.training.exercise3.generics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GenericLoop {

	public static void main(String[] args) {

		//set using for each loop
		Set<String> set = new HashSet<>();
		set.add("one");
		set.add("two");
		set.add("three");
		for (String words : set) {
			System.out.println("the words are " + words);
		}

		//list using for each loop
		List<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		for (Integer number : list) {
			System.out.println("the numbers are " + number);
		}

		//map using for each loop
		HashMap<String , String> map = new HashMap<>();
		map.put("1","one");
		map.put("2","two");
		map.put("3", "three");
		map.put("4","four");
		for (String i : map.keySet()) {
			System.out.println("key: " + i + " value: " + map.get(i));
		}
	}
}

