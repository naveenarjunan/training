/*
Requirement:
	Build a server side program using Socket class for Networking.
	
Entity:
	Server
	
Function Declaration:
	Public static void main(String[] args) {}

Jobs To Be Done:
	1) Open a SocketChannel as server.
	2) Create an object for SocketAddress as socketAddress and pass the hostName and port.
	3) Invoke connect() method and connect the socketAddress to server.
	4) Close the SocketChannel.
	
Pseudo code:
public class Client {
	
    public static void main(String[] args) throws IOException {
        
    	System.out.println("Client requesting Server");
    	
        ServerSocketChannel serverSocket = ServerSocketChannel.open();
        serverSocket.socket().bind(new InetSocketAddress(88));
        SocketChannel client = serverSocket.accept();
        
        System.out.println("Connection Set:  " + client.getRemoteAddress());
        System.out.println("Client is ready to receive");
        client.close();
    }
}
*/

package com.java.training.exercise3.networking;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.channels.SocketChannel;

public class Server {

	public static void main(String[] args) throws IOException {

		SocketChannel server = SocketChannel.open();
		SocketAddress socketAddress = new InetSocketAddress("localhost",88);
		server.connect(socketAddress);

		System.out.println("Server is ready to send");
		server.close();
	}
}