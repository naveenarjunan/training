/*
Requirement:
	Build a client side program using ServerSocket class for Networking.
	
Entity:
	Client
	
Function Declaration:
	Public static void main(String[] args) {}

Jobs To Be Done:
	1) Open a ServerSocketChannel as serverSocket.
	2) Invoke bind() and Socket() method to associate the channel with InetSocketAddress.
	3) Open a SocketChannel as client and invoke accept() method.
	4) Print the remoteAddress by invoking getRemoteAddress().
	5) Close the ServerSocketChannel.
	
Pseudo code:
public class Client {
	
    public static void main(String[] args) throws IOException {
        
    	System.out.println("Client requesting Server");
    	
        ServerSocketChannel serverSocket = ServerSocketChannel.open();
        serverSocket.socket().bind(new InetSocketAddress(88));
        SocketChannel client = serverSocket.accept();
        
        System.out.println("Connection Set:  " + client.getRemoteAddress());
        System.out.println("Client is ready to receive");
        client.close();
    }
}
*/

package com.java.training.exercise3.networking;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class Client {

	public static void main(String[] args) throws IOException {

		System.out.println("Client requesting Server");

		ServerSocketChannel serverSocket = ServerSocketChannel.open();
		serverSocket.socket().bind(new InetSocketAddress(88));
		SocketChannel client = serverSocket.accept();

		System.out.println("Connection Set:  " + client.getRemoteAddress());
		System.out.println("Client is ready to receive");
		client.close();
	}
}