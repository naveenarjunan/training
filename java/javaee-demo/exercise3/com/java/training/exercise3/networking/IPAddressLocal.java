/*Requirement: To Find out the IP Address and host name of your local computer.
 * 
 * Entity: IPAddressLocal
 * 
 * Method Signature: public static void main(String[] args);
 * 
 * Jobs to be done: 1. Find the local Host Address and print it.
 * 					2.Find the Local Host Name and print it.
 * PseudoCode:
 * 

public class IPAddressLocal {
	
	public static void main(String[] args) throws Exception {
				
		InetAddress addr = InetAddress.getLocalHost();
		System.out.println("Local HostAddress:  "+addr.getHostAddress());
		String hostname = addr.getHostName();
		System.out.println("Local host name: "+hostname);
	}
}

 */
package com.java.training.exercise3.networking;

import java.net.InetAddress;

public class IPAddressLocal {

	public static void main(String[] args) throws Exception {

		InetAddress address = InetAddress.getLocalHost();
		System.out.println("Local HostAddress:  " + address.getHostAddress());
		String hostname = address.getHostName();
		System.out.println("Local host name: " + hostname);
	}
}
