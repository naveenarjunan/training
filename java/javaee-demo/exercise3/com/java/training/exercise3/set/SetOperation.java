/*Requirement:
 *   + Create a set
         => Add 10 values
         => Perform addAll() and removeAll() to the set.
         => Iterate the set using 
           - Iterator
           - for-each
     + Explain the working of contains(), isEmpty() and give example.
     
 * Entities:
 * 		SetOperation
 * 
 * Function Declaration:
 * 		public static void main(String[] args)
 * 
 * Jobs To Be Done:
 * 		1.A set is created named set
 *      2.Values are added into the set by using add method 
 *      3.Another set is created named set1
 *      4.values of set is added to set1 by using addAll method and set1 is printed.
 *      5.remove all the elements in the set using clear() method.
 *      6.The set is iterated using iterator and it is printed
 *      7.The set is iterated using for each loop and it is printed
 *      8.To check if the set contains a certain element, contains() method is used.
 *        8.1)if it contains it prints true else it prints false. 
 *      9.To check if the set is empty or not, isEmpty() method is used.
 *        9.1)if the set is empty it prints true else it prints false. 
 *      
 *Pseudo code:
 *
 *public class SetOperation {
	public static void main(String[] args) 
	{ 

		HashSet<String> setA = new HashSet<String>();
		HashSet<String> setB = new HashSet<String>();
        
		//add the elements to the set				
	    System.out.println(setA);

		setB.addAll(setA);
		System.out.println(setB);

		setA.clear();
		System.out.println(setA);

		//iterate using iterator

		for (String element1 : setA) {
			System.out.println(element1);
		}

		boolean containsElement = setA.contains("one");
		System.out.println(containsElement);

		boolean empty = setA.isEmpty();
		System.out.println(empty);
	}


}
 *     
 */

package com.java.training.exercise3.set;

import java.util.HashSet;
import java.util.Iterator;

public class SetOperation {
	public static void main(String[] args) { 

		HashSet<String> setA = new HashSet<String>();
		HashSet<String> setB = new HashSet<String>();

		setA.add("one"); 
		setA.add("two"); 
		setA.add("three"); 
		setA.add("four"); 
		setA.add("five"); 
		setA.add("six"); 
		setA.add("seven"); 
		setA.add("eight"); 
		setA.add("nine"); 
		setA.add("ten"); 

		System.out.println(setA);

		setB.addAll(setA);
		System.out.println(setB);

		setA.clear();
		System.out.println(setA);

		Iterator<String> element=setA.iterator();
		while(element.hasNext())
		{
			System.out.println(element.next());
		}

		for (String element1 : setA) {
			System.out.println(element1);
		}

		boolean containsElement = setA.contains("one");
		System.out.println(containsElement);

		boolean empty = setA.isEmpty();
		System.out.println(empty);
	}


}
