/*
Requirement:
    To create a stack using generic type and implement
      1)Push atleast 5 elements.
      2)Pop the peek element.
      3)Search a element in stack and print the index value.
      4)print the size of stack.
      5)print the elements using stream.      
Entity:
    StackOperation

Function Declaration:
    public static void main(String[] args)

Jobs To Be Done:
    :1.A stack is created  named stack
 *                 2.Add the elements to the stack.
 *                 4.The stack is printed.
 *                 5.pop the peek element from the stack.
 *                 6.Print the peek element.
 *                 7.To find the index of an element use the search() method and print it.
 *                 8.Print the size of the stack using size() method.
 *                 10.Create a object for Stream named stream and store the stack as stream.
 *                 11.For each element in the stream
 *                    11.1.Print the element.
 *                    
 *Pseudo code:
 *
 *public class StackOperation {

    public static void main(String[] args) {
    	Stack<Integer> stack = new Stack<Integer>();
		
		//elements pushed into the stack
		
		//pop the peek element
	    stack.pop();
	   
	    //print index value of the element
	    stack.search(2);
	   
	    //size of the stack
	    stack.size();
	   
	    //printing elements using stream
	    System.out.println("Printing elements using stream:");
        Stream<Integer> stream = stack.stream();
	    stream.forEach((value) -> System.out.println(value));
    }

}
*/

package com.java.training.exercise3.stack.queue;

import java.util.Stack;
import java.util.stream.Stream;

public class StackOperation {

	public static void main(String[] args) {
		Stack<Integer> stack = new Stack<Integer>();

		//elements pushed into the stack
		for(int number = 0; number < 5; number++) { 
			stack.push(number); 
		}

		//pop the peek element
		System.out.println("pop operation:"+stack.pop());

		//print index value of the element
		System.out.println("index of element 2:"+stack.search(2));

		//size of the stack
		System.out.println("Lenght of the stack:"+stack.size());

		//printing elements using stream
		System.out.println("Printing elements using stream:");
		Stream<Integer> stream = stack.stream();
		stream.forEach((value) -> System.out.println(value));
	}

}
