/*Requirement:Create a queue using generic type and in LinkedList
                  -> add atleast 5 elements
                  -> remove the front element
                  -> search a element in stack using contains 
                     key word and print boolean value value
                  -> print the size of stack
                  -> print the elements using Stream 
 * 
 * Entity:PriorityQueueDemo
 * 
 * Function Declaration:public static void main(String[] args)
 * 
 * Jobs To Be Done:1.A linkedList queue is created  named queue.
 *                 2.Add the elements to the queue.
 *                 4.The queue is printed
 *                 5.Remove the first element in the queue.
 *                 6.Print the removed element.
 *                 7.Check whether the particular element is present in the queue.
 *                   7.1)Then print as true.
 *                   7.2)Otherwise print as false.
 *                 8.Print the size of the queue
 *                 10.Create a object for Stream named stream and store the queue as stream.
 *                 11.For each element in the stream
 *                    11.1.Print the element.
 *                    
 *Pseudo code:
  public class LinkedListDemo {

	public static void main(String[] args) {

	    Queue<Integer> queue = new PriorityQueue<>();

		//add the elements to the queue.

	    int removedElement = queue.remove(); 
	    System.out.println("removed element-" + removedElement); 


	    System.out.println(queue.contains(element)); 

	    System.out.println(queue.size()); 

	    Stream<Integer> stream = queue.stream();
	    stream.forEach((number) -> System.out.println(number));
	}
}
 */

package com.java.training.exercise3.stack.queue;

import java.util.LinkedList;
import java.util.Queue;
import java.util.stream.Stream;

public class LinkedListDemo {

	public static void main(String[] args) {

		Queue<Integer> queue = new LinkedList<>(); 

		//adding elements to queue
		queue.add(11);
		queue.add(12);
		queue.add(17);
		queue.add(15);
		queue.add(19);
		queue.add(18);
		System.out.println("Elements of queue "+ queue); 

		//remove first element
		int removedElement = queue.remove(); 
		System.out.println("removed element-" + removedElement); 

		//printing the boolean value for contains
		System.out.println(queue.contains(1)); 

		//printing the size of the queue
		System.out.println("Size of queue:" + queue.size()); 

		//printing the elements using stream
		Stream<Integer> stream = queue.stream();
		stream.forEach((number) -> System.out.println(number));
	}
}
