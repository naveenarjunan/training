/*Requirement:Consider a following code snippet
                Queue bike = new PriorityQueue();    
                bike.poll();
                System.out.println(bike.peek());    

           what will be output and complete the code.
 * 
 * Entity:QueueDemo
 * 
 * Function Declaration:public static void main(String[] args)
 * 
 * Output : null
 */

package com.java.training.exercise3.stack.queue;

import java.util.PriorityQueue;
import java.util.Queue;

public class QueueDemo {

	public static void main(String[] args) {

		Queue<?> bike = new PriorityQueue<Object>();    
		bike.poll();
		System.out.println(bike.peek());    
	}


}