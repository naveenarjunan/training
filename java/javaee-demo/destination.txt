The Java OutputStream class, java.io.OutputStream, is the 
base class of all output streams in the Java IO API. Subclasses 
of OutputStream include the Java BufferedOutputStream and the 
Java FileOutputStream among others. To see a full list of output 
streams, go to the bottom table of the Java IO Overview page.