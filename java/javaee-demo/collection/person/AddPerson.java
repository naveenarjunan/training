/*
 * Requirement:
 *      List<Person> newRoster = new ArrayList<>();
            newRoster.add(
                new Person(
                "John",
                IsoChronology.INSTANCE.date(1980, 6, 20),
                Person.Sex.MALE,
                "john@example.com"));
            newRoster.add(
                new Person(
                "Jade",
                IsoChronology.INSTANCE.date(1990, 7, 15),
                Person.Sex.FEMALE, "jade@example.com"));
            newRoster.add(
                new Person(
                "Donald",
                IsoChronology.INSTANCE.date(1991, 8, 13),
                Person.Sex.MALE, "donald@example.com"));
            newRoster.add(
                new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
        - Create the roster from the Person class and add each person in the newRoster to the 
          existing list and print the new roster List.
        - Print the number of persons in roster List after the above addition.
        - Remove the all the person in athe roster list

 * Entity:
      AddPerson

 * Function Signature:
      public static void main(String[] args) 

 * Jobs to be done:
 *    1)Invoke the method createRoaster from Person and store it in roster.
 *    2)Craete a new list named newRoster of type person.
 *    3)Add the persons to the newRoster.
 *    4)Add the newRoster persons to the roster.
 *    5)Print the roster.
 *    6)Print the size of the roster.
 *    7)Clear the roster.
 *    8)Print the roster.
 *    
 *    
 * Pseudo Code:
 * public class AddPerson {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        List<Person> newRoster = new ArrayList<>();

        // add elements to the newRoster

        Stream<Person> stream = roster.stream();

        stream.forEach(person -> System.out.println(person.name + "\t" + person.birthday + "\t"
                + person.gender + "\t" + person.emailAddress));
        System.out.println(roster.size());
        roster.clear();
        System.out.println(roster);
    }

}  

 */
package person;

import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class AddPerson {

	public static void main(String[] args) {
		List<Person> roster = Person.createRoster();
		List<Person> newRoster = new ArrayList<>();
		newRoster.add(new Person("Isha", IsoChronology.INSTANCE.date(1980, 6, 20), Person.Sex.MALE,
				"isha@example.com"));
		newRoster.add(new Person("Faf", IsoChronology.INSTANCE.date(1990, 7, 15),
				Person.Sex.FEMALE, "faf@example.com"));
		roster.addAll(newRoster);
		Stream<Person> stream = roster.stream();
		stream.forEach(person -> System.out.println(person.name + "\t" + person.birthday + "\t"
				+ person.gender + "\t" + person.emailAddress));
		System.out.println("Number of person in the roster list :" + roster.size());
		roster.clear();
		System.out.println("After removing all elements from the list :" + roster);
	}

}