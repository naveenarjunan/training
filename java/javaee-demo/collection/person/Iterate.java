/*
 * Requirement:
 *      Iterate the roster list in Persons class and and print the person without using forLoop/Stream   
 * Entity:
 *      Iterate
 * Function Signature:
 *      public static void main(String[] args)
 * Jobs to be done:
 *      1) Create the List with reference to precreated method in person.java file.
 *      2) by using iterate method print the list details one by one.
 * 
 * Pseudocode:
 * 
 * public class Iterate {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        Iterator<Person> iterator = roster.iterator();
        while (iterator.hasNext()) {
            //iterate person
            System.out.println(person.name + " " + person.birthday + " " + person.gender + " "
                    + person.emailAddress);
        }
    }
 }
 
 */
package person;

import java.util.Iterator;
import java.util.List;

public class Iterate {

	public static void main(String[] args) {
		
		List<Person> roster = Person.createRoster();
		Iterator<Person> iterator = roster.iterator();
		while (iterator.hasNext()) {
			Person person = iterator.next();
			System.out.println(person.name + " " + person.birthday + " " + person.gender + " "
					+ person.emailAddress);
		}
	}
}