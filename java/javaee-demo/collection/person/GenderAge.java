/*Requirement:
 * 		Write a program to filter the Person, who are male and age greater than 21
 * 
 * Entity:
 *		GenderAge
 *		Person
 *
 *Function Declaration:
 *		public static void main(String[] args) { }
 *      public static List<Person> createRoster() { }
 *      public Sex getGender() { }
 *      public int getAge() { }
 *      
 *Jobs To Be Done:
 *		1)Invoke the method createRoaster which returns list
 *      2)Store the returned list in list.
 *      3)Iterate the list using stream
 *        3.1)The person is filtered if gender is male and age is greater than 21.
 *        3.2)Then the filtered name are printed.
 *        
 *pseudo code:
 	public class GenderAge {
	 
		public static void main(String[] args) {
	       
		    List<Person> list = Person.createRoster();  
	        
	        use stream iterate the list.
	        filter(person -> (person.getGender() == Person.Sex.MALE ))
	        filter(person -> (person.getAge() > 21))
	        print the filtered person name
	        
	   }
    }
*/
package person;

import java.util.List;

public class GenderAge {

	public static void main(String[] args) {

		List<Person> list = Person.createRoster();  

		list.stream()
		.filter(person -> (person.getGender() == Person.Sex.MALE ))
		.filter(person -> (person.getAge() > 21))
		.forEach(print -> System.out.println(print.getName()));
	}
}