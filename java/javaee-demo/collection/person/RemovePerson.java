/*
 * Requirements:
 *    List<Person> newRoster = new ArrayList<>();
            newRoster.add(
                new Person(
                "John",
                IsoChronology.INSTANCE.date(1980, 6, 20),
                Person.Sex.MALE,
                "john@example.com"));
            newRoster.add(
                new Person(
                "Jade",
                IsoChronology.INSTANCE.date(1990, 7, 15),
                Person.Sex.FEMALE, "jade@example.com"));
            newRoster.add(
                new Person(
                "Donald",
                IsoChronology.INSTANCE.date(1991, 8, 13),
                Person.Sex.MALE, "donald@example.com"));
            newRoster.add(
                new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
* Remove the following person from the roster List:
            new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
 
 * Entities:
 *    RemovePerson
 *    
 * Function Signature:
 *    public static void main(String[] args) 
 *    
 * Jobs To Be Done:
 *    1)Create a ArrayList and Add the person details to the newRoaster.
 *    2)Print the newRoster using stream.
 *    3)Remove the person "Bob" in roaster .
 *      3.1)print the roster using stream
 *    
 *    
 * Pseudo code:
 * public class RemovePerson {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        List<Person> newRoster = new ArrayList<>();
        //Add all the person details
        Stream<Person> stream = newRoster.stream();
        stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
                + person.gender + " " + person.emailAddress));
        //remove the person Bob from roster.
        Stream<Person> stream1 = roster.stream();
        stream1.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
                + person.gender + " " + person.emailAddress));

    }

}
 * 
 *
 *
 */
package person;

import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RemovePerson {

	public static void main(String[] args) {

		List<Person> roster = Person.createRoster();
		List<Person> newRoster = new ArrayList<>();
		newRoster.add(new Person("John", IsoChronology.INSTANCE.date(1980, 6, 20), Person.Sex.MALE,
				"john@example.com"));
		newRoster.add(new Person("Jade", IsoChronology.INSTANCE.date(1990, 7, 15),
				Person.Sex.FEMALE, "jade@example.com"));
		newRoster.add(new Person("Donald", IsoChronology.INSTANCE.date(1991, 8, 13),
				Person.Sex.MALE, "donald@example.com"));
		newRoster.add(new Person("Bob", IsoChronology.INSTANCE.date(2000, 9, 12), Person.Sex.MALE,
				"bob@example.com"));
		Stream<Person> stream = newRoster.stream();
		stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
				+ person.gender + " " + person.emailAddress));
		System.out.println(" ");
		roster = roster.stream().filter(element -> element.getName() != "Bob")
				.collect(Collectors.toList());
		Stream<Person> stream1 = roster.stream();
		stream1.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
				+ person.gender + " " + person.emailAddress));

	}

}
