/*Requirement:
 *      Consider the following Person:
 *          new Person(
 *              "Bob",
 *              IsoChronology.INSTANCE.date(2000, 9, 12),
 *              Person.Sex.MALE, "bob@example.com"));
 *     - Check if the above person is in the roster list obtained from Person class.
 * 
 * Entity:
 * 		Person
 *      CheckPerson
 * 
 * Method Signature:
 *      public static void main(String[] args)
 * 
 * Jobs To Be Done:
 *      1) Declare the boolean value is false.
 *      2) Create an object that can able to access the predefined method in Person.java.
 *      3) Create an object for the given person.
 *      4) Create a reference for the Predefined method using Stream. 
 *      5) Checks Whether the person is present or not
 *           5.1) If the given person is present in person.java, the present value is true.
 *      6) Checks Whether present value is true or false 
 *           6.1) If the present value is equals to true, print " Person is present"
 *           6.2) If the present value is not equals to true, print " Person is not present".
 * Pseudo Code:
 * 
 *  class CheckPerson {

    public static boolean present = false;

    public static void main(String[] args) {
       //Referring from Person.java
        List<Person> roster = Person.createRoster();
        //Given person
        Person newPerson = new Person("Bob", IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com");
        //Create a Reference Using Stream
        //Checks whether the given person is present or not
        if (present == true) {
            System.out.println("Person is present");
        } else {
            System.out.println("Person is not present");
        }

    }
}

*/
package person;

import java.time.chrono.IsoChronology;
import java.util.List;
import java.util.stream.Stream;

public class CheckPerson {

	public static boolean present = false;

	public static void main(String[] args) {

		List<Person> roster = Person.createRoster();
		Person newPerson = new Person("Bob", IsoChronology.INSTANCE.date(2000, 9, 12),
				Person.Sex.MALE, "bob@example.com");
		Stream<Person> stream = roster.stream();
		stream.forEach(person -> {
			if ((person.getName().equals(newPerson.getName()))
					&& (person.getBirthday().equals(newPerson.getBirthday()))
					&& (person.getEmailAddress().equals(newPerson.getEmailAddress()))
					&& (person.getGender().equals(newPerson.getGender()))) {
				present = true;
			}
		});
		if (present == true) {
			System.out.println("Person is present");
		} else {
			System.out.println("Person is not present");
		}

	}
}