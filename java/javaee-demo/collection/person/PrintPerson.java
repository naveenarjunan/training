/*
 * Requirement:
 *      Print all the persons in the roster using java.util.Stream<T>#forEach 
 *
 * Entity:
 *      PrintPerson
 * 
 * Function Signature:
 *      public static void main(String[] args)
 * 
 * Jobs to be done:
 *      1)Invoke the method createRoaster from Person and store it in list
 *      2)Create a Stream named stream and store the list as stream.
 *      3)For each person in the stream
 *         3.1)Print the name , birthday , gender and emailAddress 
 *      
 * Pseudo Code:
 * public class PrintPerson {

	public static void main(String[] args) {

		List<Person> list = Person.createRoster();
		Stream<Person> stream = list.stream();
		stream.forEach(person -> System.out.println(person.name + "\t" + person.birthday + "\t"
				+ person.gender + "\t" + person.emailAddress));
	}
}
 */
package person;

import java.util.List;
import java.util.stream.Stream;

public class PrintPerson {

	public static void main(String[] args) {

		List<Person> list = Person.createRoster();
		Stream<Person> stream = list.stream();
		stream.forEach(person -> System.out.println(person.name + "\t" + person.birthday + "\t"
				+ person.gender + "\t" + person.emailAddress));
	}
}