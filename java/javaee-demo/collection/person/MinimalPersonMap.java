/*
 * Requirement:
 *      To print minimal person with name and email address from the Person class using java.util.Stream<T>#map API by referring Person.java
 * 
 * Entity:
 * 		Person
 *      MinimalPersonMap
 *
 * Function Signature:
 *      public static void main(String[] args)
 *
 * Jobs To Be Done:
 *      1)Create a object that can access the predefined list in Person.java
 *      2)Create a ArrayList of type String.
 *          2.1)Get the Person name using stream mapping .
 *      3)Create a ArrayList of type String.
 *          3.1)Get the mailId using stream mapping.
 *      4)Create minimalname of type String and get the minimal name in name.
 *      5)Create minimalId of type String and get the minimalId in mailId
 *      6)Print the minimalname and minimalId.
 *      
 * Pseudo code:
 * public class MinimalPersonMap {

   		public static void main(String[] args) {
    		List<Person> roster = Person.createRoster();
        	ArrayList<String> name = new ArrayList<>();
        	ArrayList<String> mailId = new ArrayList<>();
        	
        	//Get the Person name using stream mapping 
        	//Get the Person mailId using stream mapping 
        	String minimalName = Collections.min(name);
        	String minimalId = Collections.min(mailId);

        	System.out.println(
                "The Minimal Person Name is " + minimalName + " And EmailId is " + minimalId);
    	}
	}

 * 
 */
package person;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class MinimalPersonMap {

	public static <R, T> void main(String[] args) {
		
		List<Person> roster = Person.createRoster();
		ArrayList<String> name = (ArrayList<String>) roster.stream().map(s -> s.getName())
				.collect(Collectors.toList());
		ArrayList<String> mailId = (ArrayList<String>) roster.stream().map(s -> s.getEmailAddress())
				.collect(Collectors.toList());
		String minimalName = Collections.min(name);
		String minimalId = Collections.min(mailId);

		System.out.println(
				"The Minimal Person Name is " + minimalName + " And EmailId is " + minimalId);
	}
}
