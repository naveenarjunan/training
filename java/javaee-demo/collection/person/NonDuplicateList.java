/*
 * Requirement:
 *       Consider a following code snippet:
 *   List<Integer> randomNumbers = Array.asList({1, 6, 10, 1, 25, 78, 10, 25})
 *      - Get the non-duplicate values from the above list using java.util.Stream API
 * Entity:
 *     NonDuplicateList
 * Function Declaration:
 *     public static void main(String[] args)
 * Jobs to be done:
 *    1)Create a list randomNumbers and store elements in it.
 *    2)Create a list withoutDuplicate 
 *       2.1)For each person in the list randomNUmbers.
 *           2.1.1)Filter a element whose frequency is one.
 *           2.1.2)Collect those elements as list.
 *    3)Create a Stream named stream and store the withoutDuplicate list as stream in it.
 *    4)For each element in the stream.
 *      4.1)Print the element
 *
 *Pseudo code:
 *
 public class NonDuplicateList {
    public static void main(String[] args) {
    
        List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);
        List<Integer> withoutDuplicate = randomNumbers.stream()
                .filter(element -> Collections.frequency(randomNumbers, element) == 1)
                .collect(Collectors.toList());
        Stream<Integer> stream = withoutDuplicate.stream();
        stream.forEach(elements -> System.out.print(elements + " "));
    }
}

 */
package person;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NonDuplicateList {

	public static void main(String[] args) {

		List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);
		List<Integer> withoutDuplicate = randomNumbers.stream()
				.filter(element -> Collections.frequency(randomNumbers, element) == 1)
				.collect(Collectors.toList());
		Stream<Integer> stream = withoutDuplicate.stream();
		stream.forEach(elements -> System.out.print(elements + " "));
	}
}