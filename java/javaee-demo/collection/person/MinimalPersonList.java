/*
 * Requirement:
 *      To print minimal person with name and email address from the Person class using java.util.Stream<T> API by referring Person.java
 * 
 * Entity:
 *      MinimalPersonList
 *      Person
 *
 * Function Declaration:
 *      public static void main(String[] args)
 *
 * Jobs To Be Done:
 *      1) Create the List with reference to precreated method in person.java file.
 *      2) Create two arraylist.
 *      	2.1)with an reference object name as name.
 *      	2.2)with an reference object name as mailId.
 *      3) by using for loop, Get the name and mailId from precreated method and store it in array.
 *      4) Find min name and mail Id from the collections using min() method.
 *      5)print the name and the corresponding mail id.
 *      
 * Pseudocode:
 * 
 * public class MinimalPersonList {
    
       public static void main(String[] args) {
        	List<Person> roster = Person.createRoster(); 
    		ArrayList<String> name = new ArrayList<>();
        	ArrayList<String> mailId = new ArrayList<>(); 
        	for (Person p : roster) {
                 //get name 
                 //get mailId
        	}
        	Collections.min(name);
        	Collections.min(mailId);

        	System.out.println(
                "The Minimal Person Name is " + minimalName + " And EmailId is " + minimalId);
    	}

	}
*/
package person;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MinimalPersonList {

	public static void main(String[] args) {
		List<Person> roster = Person.createRoster();
		ArrayList<String> name = new ArrayList<>();
		ArrayList<String> mailId = new ArrayList<>();
		for (Person p : roster) {
			name.add(p.getName());
			mailId.add(p.getEmailAddress());
		}
		String minimalName = Collections.min(name);
		String minimalId = Collections.min(mailId);

		System.out.println(
				"The Minimal Person Name is " + minimalName + " And EmailId is " + minimalId);
	}

}