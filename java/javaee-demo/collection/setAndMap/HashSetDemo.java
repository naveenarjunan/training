/*
Requirement:
    java program to demonstrate adding elements, displaying, removing, and iterating in hash set
Entity:
    HashSetDemo
Function Declaration:
    public static void main(String[] args){} 
Jobs To Be Done:
    1)Create object for HashSet.
    2)Add elements to the hashSet.
    3)Iterate the elements in the hashSet.
    4)Display the elements in the hashSet.
    5)Remove the elements in the hashSet.
    
Pseudo code:
    public class HashSetDemo {
        public static void main(String[] args) {
            //Creating a new HashSet
            //Adding elements to the Hashset
            //Iterating the elements in the Hashset
            //removing the elements in the Hashset
        }
    }
*/
package setAndMap;

import java.util.HashSet;

public class HashSetDemo {

	public static void main(String[] args) {
		HashSet<Integer> set = new HashSet<>();

		set.add(10);
		set.add(20);
		set.add(30);
		set.add(40);
		System.out.println("Displaying element in set " + set);
		set.remove(10);
		System.out.println("Printing the set after removing the elemnt " + set);
		System.out.println("Displaying element using iterating method ");
		for (int element : set) {
			System.out.println(element + " ");
		}
	}

}
