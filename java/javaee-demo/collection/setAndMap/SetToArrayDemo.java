/*
Requirement:
    demonstrate linked hash set to array() method in java
Entity:
    SetToArrayDemo
Function Declaration:
    public static void main(String[] args){} 
Jobs To Be Done:
    1)Create object for LinkedHashSet as set.
    2)Add elements to the linkedHashSet
    3)copy the elements of linkedHashSet to the new Array.
    4)print the elements of array.
    
Pseudo code:
    public class SetToArrayDemo {
        public static void main(String[] args) {
            LinkedHashSet<String> set = new LinkedHashSet<>();
            //Add elements to the linkedHashHashset
            //create new array and copy elements
            //print the array using for loop
        }
    }
 */

package setAndMap;
import java.util.LinkedHashSet;

public class SetToArrayDemo {
	
	public static void main(String[] args) {
		
		LinkedHashSet<String> set = new LinkedHashSet<>();
		set.add("one");
		set.add("two");
		set.add("three");
		set.add("four");
		set.add("five");
		System.out.println("The Linked Hash Set " + set);
		Object[] array = set.toArray();
		System.out.println("The array is");
		for(Object name : array) {
			System.out.println(name + " ");
		}

	}

}