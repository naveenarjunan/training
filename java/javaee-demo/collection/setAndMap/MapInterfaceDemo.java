/*
 * Requirement:
 *      demonstrate java program to working of map interface using( put(), remove(), booleanContainsValue(), replace() )
 * Entity:
 *      MapInterfaceDemo
 * Function declaration:
 *      public static void main(String[] args) 
 * Jobs to be done:
 *      1)Create a class named MapInterfaceDemo
 *      2)Create reference for TreeMap and name it as map.
 *      3)add elements to the map.
 *      4)Perform put(),remove(),replace(),containsValue() method in it.
 *      
 * Pseudo code:
		public class MapInterfaceDemo {

			public static void main(String[] args) {
				//create a new treemap
				//add elements to the map
				//remove an element from the map
				//replace an element in the map
				//check whether it contains a certain element in it.
			}
		}
*/

package setAndMap;

import java.util.TreeMap;

public class MapInterfaceDemo {

	public static void main(String[] args) {

		TreeMap<Integer, String> map = new TreeMap<>();
		map.put(1, "one");
		map.put(2, "two");
		map.put(3, "three");
		map.put(4, "four");
		map.put(5, "five");
		System.out.println("The map elements are " + map);
		map.remove(5);
		System.out.println("After removing element the map is " + map);
		System.out.println(map.containsValue("three"));
		map.replace(4, "four");
		System.out.println("After replacing the key and value of map " + map);
	}
}
