/* Requirement:
 * 	java program to demonstrate insertions and string buffer in tree set.
 * 
 * Entity:
 * 	TreeSetDemo
 * 
 * Function Declaration:
 *  public static void mian(String[] args) {} 
 * 
 *Jobs To Be Done:
 *  1)create a treeset
 *  2)Add the elements to the treeset as stringbuffer
 * 
 * Pseudo code:
 *  
 *  public class StringBUfferInsert {
 *   
 *      public static void main(String[] args) {
 *      
 *      	TreeSet<StringBuffer> treeSet = new TreeSet<>();
 *          //add elements to TreeSet as StringBuffer
 *      }
 * }
 *  
 * 
 */
package setAndMap;

import java.util.TreeSet;

public class TreeSetDemo {

	public static void main(String[] args) {

		TreeSet<StringBuffer> treeSet = new TreeSet<>();
		treeSet.add(new StringBuffer("A"));
		treeSet.add(new StringBuffer("B"));
		treeSet.add(new StringBuffer("C"));
		treeSet.add(new StringBuffer("D"));
		treeSet.add(new StringBuffer("E"));
		System.out.println(treeSet);
	}

}
/*
Explanation:
String class and all the Wrapper classes already implements Comparable interface but StringBuffer 
class doesn�t implements Comparable interface. Hence, we get a ClassCastException in the above 
example.*/