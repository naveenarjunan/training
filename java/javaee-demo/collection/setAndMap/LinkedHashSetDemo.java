/*
Requirement:
    demonstrate program explaining basic add and traversal operation of linked hash set
Entity:
    LinkedSetDemo
Function Declaration:
    public static void main(String[] args){} 
Jobs To Be Done:
    1)Create object for LinkedHashSet named as set.
    2)Add elements to the set.
    3)Iterate the elements of linkedHashSet.
    4)print the set
    
Pseudo code:
    public class SetDemo {
        public static void main(String[] args) {
            Set<String> linkedHashSet = new LinkedHashSet<String>();
            //Iterating the elements in the linkedHashHashset
        }
    }
*/
package setAndMap;

import java.util.Iterator;
import java.util.LinkedHashSet;

public class LinkedHashSetDemo {

	public static void main(String[] args) {
		LinkedHashSet<Integer> set = new LinkedHashSet<>();
		set.add(1);
		set.add(2);
		set.add(3);
		set.add(4);
		set.add(5);
		System.out.println("Set elements are " + set);
		System.out.println("Set elements are using iterator");
		Iterator<Integer> iterator = set.iterator();
		while (iterator.hasNext()) {
			System.out.print(iterator.next() + " ");
		}
		System.out.println();
	}
}
