/*
 * Requirements:
 *    LIST CONTAINS 10 STUDENT NAMES
      krishnan, abishek, arun,vignesh, kiruthiga, murugan,adhithya,balaji,vicky, priya and 
      display only names starting with 'A'.
    
 * Entities:
 *    StudentsDemo
 *    
 * Function Declaration:
 *    public static void main(String[] args)
 *    
 * Jobs To Be Done:
 *    1.Create a class named as StudentsDemo.
 *    2.Create an arraylist and name the object as list.
 *    3.add the elements into the list.
 *    4.the list the values are converted into uppercase.
 *    5.Using for each loop and if condition the names are startswith "A" prints the name.
 *    
 * Pseudo code:

		public class StudentsDemo {

			public static void main(String[] args) {

				List<String> list = new ArrayList<>();
				//add elements to the list
				//print the names that startswith "A"
			}
		}

*/ 
    	
package collection;

import java.util.List;
import java.util.ArrayList;

public class StudentsDemo {

    public static void main(String[] args) {
    	
    	List<String> list = new ArrayList<>();
        list.add("krishnan");
        list.add("abishek");
        list.add("arun");
        list.add("vignesh");
        list.add("kiruthiga");
        list.add("murugan");
        list.add("adhithya");
        list.add("balaji");
        list.add("vicky");
        list.add("priya");
       
        list.replaceAll(String::toUpperCase);
        System.out.println("list  " + list);      
        
        for (String names : list) {
            if (names.startsWith("A")) {
                System.out.println(names);
            }
        }
    }
}

