/*
Requirement:
    Add and remove the elements in stack.      
Entity:
    StackDemo

Function Declaration:
    public static void main(String[] args)

Jobs To Be Done:
    1)Create an object named sample with stack class.
    2)Push any 5 elements into the stack.
    3)Removing the some of the elements using "remove()" , then print the sample .
       
Pesudo code:
 public class StackDemo {
    public static void main(String[] args) {
        Stack<Integer> sample = new Stack<>();
        //add elements to the stack and print it.
                 
        //remove elements from the stack
        System.out.println("After removing the some of element :" + sample);
    }
}

*/
package collection;

import java.util.Stack;

public class StackDemo {
	
    public static void main(String[] args) {
    	
        Stack<Integer> sample = new Stack<>();
        sample.push(12);
        sample.push(14);
        sample.push(16);
        sample.push(18);
        sample.push(77);
        System.out.println("Before removing the some of element :" +sample);
        
        sample.remove(2);
        sample.remove(1);
        System.out.println("After removing the some of element :" + sample);
    }
}

   