/*
Requirement:
    what is the difference between poll() and remove() method of queue interface?
Entity:
    QueueInterfaceDemo
Function Declaration:
    public static void main(String[] args){} 
Jobs To Be Done:
    1)Create an empty Queue as queue.
    2)poll() method returns null.
    3)remove() method throws exception.
    
    
Pseudo code:
    public class QueueInterfaceDemo {
        public static void main(String[] args) {
            Queue<String> queue = new Queue<String>();
            queue.poll();
            queue.remove();
        }
    }

*      
* Solution:
* poll(): 
*      ->This method is used to retrieve the head element of the Queue or in other words,
*           it is used to retrieve the first element or initial element of the queue.
*      ->In the case of poll() method, it retrieves the head element of the queue and then 
*      		removes the head element of the queue.
*      ->In case of poll() method if the queue is empty then it will return null but it does 
*      		not throw an exception.
*     -> The return type of this method is not void that means this method return first element 
*     		of the Queue.
*  
*remove():
*      ->This method is used to remove the head element of the Queue and retrieve the 
*      		first element of * the Queue like poll() method.
*      ->In the case of remove() method, it retrieves the head element and removes 
*      		the first element of * the Queue as well by calling remove() method.
*      ->In case of remove() method, if Queue is empty then, in that case, it throws an
*      		NoSuchElementFoundException but it does not return null like of poll() method.
*

*/
package collection;

import java.util.PriorityQueue;
import java.util.Queue;

public class PollAndRemove {

	public static void main(String[] args) {

		Queue<Integer> queue = new PriorityQueue<>();
		queue.add(1);
		queue.add(2);
		queue.add(3);
		queue.add(4);
		queue.add(5);
		queue.add(6);
		System.out.println("Before using poll :" + queue);
		queue.poll();
		System.out.println("After using poll :" + queue);

		System.out.println("Before using remove :" + queue);
		queue.remove();
		System.out.println("After using remove :" + queue);
		queue.clear();
		System.out.println(queue.remove()); // throws exception
	}
}

