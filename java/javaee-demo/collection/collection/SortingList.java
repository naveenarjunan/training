/*
 * Requirements : 
 *      Code for sorting vector list in descending order
 * Entities :
 *      SortingList.
 * Function Declaration :
 *      public static void main(String[] args)
 * Jobs To Be Done:
 *      1)In collection package, importing the Arraylist and Collections.
 *      2)Creating the ReverseList class.
 *      3)Creating the list object and appending the list values.
 *      4)Print the original list.
 *      5)Reverse the list by using the function "reverseOrder() ".
 *      9)Printing the reversed list.
 */
package collection;

import java.util.ArrayList;
import java.util.Collections;

public class SortingList {
	
    public static void main(String[] args) {
    	
        ArrayList<Integer> list = new ArrayList<>();
        list.add(10);
        list.add(20);
        list.add(30);
        list.add(40);
        list.add(50);
        list.add(60);
        list.add(70);
        System.out.println("Before Reversing : " + list);

        Collections.sort(list, Collections.reverseOrder());
        System.out.println("After Reversing :" + list);
    }
}
