/*
 * Requirements:
 *    Create an array list with 7 elements, and create an empty linked list add all elements of the 
 *    array list to linked list ,traverse the elements and display the result.
 * 
 * Entities:
 *    LinkedListDemo
 *
 * Function Declaration:
 *    public static void main(String[] args)
 *    
 * Jobs To Be Done:
 *    1.Create the class LinkedListDemo.
 *    2.Create the object arrayList as Generic type.
 *    3.Add the values in arrayList.
 *    4.Prints the arrayList elements.
 *    5.create the object linkedList as Generic type.
 *    6.Add all the elements of the arrayList to linkedList.
 *    7.Using forEach loop, prints the linkedList elements. 
 *    
 * Pseudo code:
 *  
 *  public class LinkedListDemo {
 *   
 *      public static void main(String[] args) {
 *      
 *      	ArrayList<Integer> arrayList = new ArrayList<>();
 *          //add elements to the ArrayList
 *          
 *          LinkedList<Integer> linkedList = new LinkedList<>();
            linkedList.addAll(arrayList);
            
            //print the values using for each loop
 *      }
 * }
 */

package collection;

import java.util.ArrayList;
import java.util.LinkedList;

public class LinkedListDemo {

    public static void main(String[] args) {
    	
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(4);
        arrayList.add(5);
        arrayList.add(6);
        arrayList.add(7);
        System.out.println("Array list elements are " + arrayList);
        LinkedList<Integer> linkedList = new LinkedList<>();
        linkedList.addAll(arrayList);
        System.out.println("Linkedlist elements are using forEach");
        for (int elements : linkedList) {
            System.out.print(elements + " ");
        }
    }

}

