/*
 * Requirement:
 *     Use addFirst(),addLast(),removeFirst(),removeLast(),peekFirst(),peekLast(),
 *     pollFirst(),poolLast() methods to store and retrieve elements in ArrayDequeue .
 *     
 * Entity:
 *     ArrayDequeDemo
 *     
 * Function:
 *     public static void main(String[] args) 
 *     
 * Jobs to be Done;
 *     1)Create the class ArrayDequeDemo.
 *     2)Create the reference for ArrayDeque as deque.
 *     3)Add the elements to the deque.
 *     4)Add the first element to the deque using addFirst() method and then print the deque.
 *     5)Add the last element to the deque using addLast() method and then print the deque.
 *     6)Removing the first and last element in the deque , then print deque.
 *     7)print the first and last peek element by using "peekFirst()" and "peekLast()" method.
 *     8)print the first and last poll element by using "pollFirst" and "pollLast" method.
 *     
 *pseudo code:
		public class ArrayDequeDemo {
			public static void main(String[] args) {

				ArrayDeque<Integer> deque = new ArrayDeque<>();
				//add elements to the deque
				deque.addFirst();
				deque.addLast();        
				deque.removeFirst();    
				deque.removeLast();
				deque.peekFirst();
				deque.peekLast();
				deque.pollFirst();
				deque.pollLast();
			}
		}
*/

package collection;

import java.util.ArrayDeque;

public class ArrayDequeDemo {
	public static void main(String[] args) {

		ArrayDeque<Integer> deque = new ArrayDeque<>();
		deque.add(1);
		deque.add(2);
		deque.add(3);
		deque.add(4);
		deque.add(5);
		deque.add(6);
		deque.add(7);
		deque.add(8);
		System.out.println("The ArrayDeque " + deque);

		deque.addFirst(123);
		System.out.println("After adding the first element" + deque);

		deque.addLast(987);
		System.out.println("After adding the last element" + deque);

		deque.removeFirst();
		System.out.println("After removing the first element" + deque);

		deque.removeLast();
		System.out.println("After removing the first element" + deque);

		System.out.println("The first peek element " + deque.peekFirst());

		System.out.println("The last  peek element " + deque.peekLast());

		System.out.println("The first poll element " + deque.pollFirst());

		System.out.println("The last poll element " + deque.pollLast());

	}

}