/*
 * Requirement:
 *      8 districts are shown below
 *  Madurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy   to be converted to UPPERCASE.
 *Entity:
 *     ToUpperCaseDemo 
 *Function Declaration:
 *     public static void main(String[] args)
 *Jobs to be done:
 *     1)Create class named as ToUpperCaseDemo.
 *     2)Create reference for list and assign all values given.
 *     3)Change all the string value in uppercase.
 *     4)Print the list.
 *     
 *Pseudo code:
 *  
 *  public class ToUpperCaseDemo {
 *   
 *      public static void main(String[] args) {
 *      
 *      	List<String> list = new ArrayList<>();
            //add the elements 
            //use toUpperCase method to be converted to UPPERCASE.
            //print the list.
 *      }
 * }      
 */
package collection;

import java.util.ArrayList;
import java.util.List;

public class ToUpperCaseDemo {

	public static void main(String[] args) {

		List<String> list = new ArrayList<>();
		list.add("Madurai");
		list.add("Coimbatore");
		list.add("Theni");
		list.add("Chennai");
		list.add("Karur");
		list.add("Salem");
		list.add("Erode");
		list.add("Trichy");

		list.replaceAll(String::toUpperCase);
		System.out.println(list);
	}

}
