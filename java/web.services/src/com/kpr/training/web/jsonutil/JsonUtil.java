package com.kpr.training.web.jsonutil;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonUtil {

public static Object jsonToObject(String json, Object typeObj) {
        
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();        
        return gson.fromJson(json, typeObj.getClass());
    }
    
    public static String objectToJson(Object object) {
        
        Gson gson = new Gson();
        String jsonValue = gson.toJson(object);
        return jsonValue;
    }
}