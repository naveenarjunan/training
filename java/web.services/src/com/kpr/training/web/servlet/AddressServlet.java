package com.kpr.training.web.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kpr.training.web.jsonutil.JsonUtil;
import com.kpr.training.web.model.Address;
import com.kpr.training.web.service.AddressService;
import com.kpr.training.web.service.ConnectionService;

public class AddressServlet extends HttpServlet {

	//doPut -> update
	//doPost -> create
	//doDelete -> delete
	//doGet -> Read

	private static final long serialVersionUID = 1L;
	Address addressClass = new Address();
	private static JsonUtil jsonUtil = new JsonUtil();
	private static AddressService addressService = new AddressService();
	private static Address addressGot = new Address();
	private static String addressString;

	protected void doGet(HttpServletRequest req, HttpServletResponse res) {

		StringBuilder builder = new StringBuilder();;
		try {

			BufferedReader reader = req.getReader();
			String line = null;
			while((line = reader.readLine()) != null) {
				builder.append(line);
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		Address add = (Address) jsonUtil.jsonToObject(builder.toString(), addressClass);
		long addressId = add.getId();
		try (PrintWriter writer = res.getWriter()){

			if(addressId != 0) {
				addressGot = addressService.read(addressId);
				addressString = jsonUtil.objectToJson(addressGot);
				writer.append(addressString);
			} else {
				writer.append("Address Read Failure");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}


	}

	protected void doPut(HttpServletRequest req, HttpServletResponse res) {

		try {
			BufferedReader reader = req.getReader();
			res.getWriter();
			StringBuilder builder = new StringBuilder();

			String line = null;

			while((line = reader.readLine()) != null) {
				builder.append(line);
			}

			Address newAddress = (Address) jsonUtil.jsonToObject(builder.toString(), addressClass);
			addressService.update(newAddress);


		} catch(Exception e) {
			e.printStackTrace();
		}

	}

	protected void doPost(HttpServletRequest req, HttpServletResponse res) {
		try {
			BufferedReader reader = req.getReader();
			PrintWriter writer = res.getWriter();
			StringBuilder addressJson = new StringBuilder();

			String line = null;

			while((line = reader.readLine()) != null) {
				addressJson.append(line);
			}

			addressGot = (Address) jsonUtil.jsonToObject(addressJson.toString(), addressClass);

			long addressId = addressService.create(addressGot);
			ConnectionService.commit(true);

			if(addressId !=0) {
				writer.append("Address Creation Success : "+ addressId);
			} else {
				writer.append("Address Creation Failure");
			}

		} catch(Exception e) {
			e.printStackTrace();
		}
	}


	protected void doDelete(HttpServletRequest req, HttpServletResponse res) {
		try {

			BufferedReader reader = req.getReader();
			StringBuilder addressGot = new StringBuilder();

			String line = null;

			while((line = reader.readLine()) != null) {
				addressGot.append(line);
			}

			Address address = (Address) jsonUtil.jsonToObject(addressGot.toString(), addressClass);

			addressService.delete(address.getId());
			ConnectionService.commit(true);

		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}