package com.kpr.training.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kpr.training.web.exception.AppException;
import com.kpr.training.web.model.Person;
import com.kpr.training.web.service.PersonService;
import com.kpr.training.web.jsonutil.JsonUtil;

public class PersonServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {

		try(PrintWriter writer = res.getWriter()) {
			writer.append(JsonUtil.objectToJson(new PersonService().create((Person) JsonUtil.jsonToObject(new StringBuffer()
					.append(req.getReader().lines().collect(Collectors.joining(System.lineSeparator()))).toString(),
					Person.class)))); 
		} catch (AppException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {

		long personId = Long.parseLong(req.getParameter("id"));
		boolean addAddress = Boolean.parseBoolean(req.getParameter("incudeAddress"));
		try (PrintWriter writer = res.getWriter()) {

			if (personId == 0 && addAddress == false) {
				writer.append(JsonUtil.objectToJson(new PersonService().readAll()));
			} else {
				writer.append(JsonUtil.objectToJson(new PersonService().read(personId, addAddress)));
			}
		} catch (AppException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void doPut(HttpServletRequest req, HttpServletResponse res) throws IOException {

		try {
			new PersonService().update((Person) JsonUtil.jsonToObject(new StringBuffer()
					.append(req.getReader().lines().collect(Collectors.joining(System.lineSeparator()))).toString(),
					Person.class));
		} catch (AppException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void doDelete(HttpServletRequest req, HttpServletResponse res) throws IOException {

		try {
			new PersonService().delete(Long.parseLong(req.getParameter("id")));
		} catch (AppException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
