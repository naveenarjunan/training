/*
Requirement:
    To demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of
    objects

Entity:
    Snake

Function Declaration:
    public void sound()

Jobs to be done:
    1. Declare the class Snake extends from Animal.
    2. Declare the method sound().
    3. Print the the statement as given.
 */

package com.java.training.exercise2.inheritance;

public class Snake extends Animal {

	public void sound() {

		System.out.println("the snake crawls");
	}
}