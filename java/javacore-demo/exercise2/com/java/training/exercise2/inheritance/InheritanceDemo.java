/*
Requirement:
    Demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class 
    of objects

Entity:
    Inheritance

Function Declaration:
    public void move()
    public void DogType()
    Public void move(int km)

Jobs To Be Done:
    1) Create a object for the Animal as animal.
    2) Create a object for the Dog as dog.
    3) Create a object for the Cat as cat.
    4) Create a object for the Snake as snake.
    5) Invoke the method sound() for the object animal, dog, snake, cat.
    6) Invoke the method run() for the object animal.
    7) Invoke the method run() along with the parameter.
    8) Print the result.
 */

package com.java.training.exercise2.inheritance;

public class InheritanceDemo{

	public static void main(String[] args) {

		Animal animal = new Animal();
		Cat cat = new Cat();
		Dog dog = new Dog();
		Snake snake = new Snake();

		dog.sound();
		snake.sound();
		cat.sound();

		animal.sound();
		animal.run();
		animal.run(10);
	}
}