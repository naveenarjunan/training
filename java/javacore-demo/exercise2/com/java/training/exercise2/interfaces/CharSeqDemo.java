/*
Requirement:
    To Write a class that implements the CharSequence interface found in the java.lang package.
    And the implementation should return the string backwards.

Entity:
    Interface

Function Declaration:
    public int length()
    public CharSequence subSequence(int i, int i1)
    public String toString()
    public char charAt(int i)
    public static void main(String[] args)

Jobs to be Done:
    1. Import CharSequence.
    2. Declare a class Interface implement CharSequence.
    3. Declare a variable charSequence of type String.
    4. Create a constructor for Interface.
    5. Get the string and store it in strArray of char[] type.
    6. Take a clone of the strArray and upto a length of the String reverse the string and print it.
    7. Invoke the methods length(), charAt(), subSequence() and toString().
    8. Print the results.
 */

package com.java.training.exercise2.interfaces;

import java.lang.CharSequence;

public class CharSeqDemo implements CharSequence {

	private String charSequence;

	public CharSeqDemo(String charSequence) {

		char[] strArray = charSequence.toCharArray();
		char[] reversedArray = strArray.clone();
		int j = strArray.length - 1;

		for (int i = 0; i < strArray.length; i++) {

			reversedArray[j] = strArray[i];
			j--;
		}

		this.charSequence = new String(reversedArray);
		System.out.println("Reverse of the String is : " + " " + this.charSequence);
	}

	public int length() {

		return charSequence.length();
	}

	public char charAt(int i) {

		return charSequence.charAt(i);
	}

	public CharSequence subSequence(int i, int i1) {

		return charSequence.subSequence(i, i1);
	}

	public String toString() {

		return charSequence;
	}

	public static void main (String[] args) {

		String string = "I AM CAPTAIN AMERICA";
		CharSeqDemo string1 = new CharSeqDemo(string);

		System.out.println("length : " + string1.length());
		System.out.println("charAt : " + string1.charAt(2));
		System.out.println("subSequence : " + string1.subSequence(1,6));
		System.out.println("toString : " + string1.toString());
	}
}