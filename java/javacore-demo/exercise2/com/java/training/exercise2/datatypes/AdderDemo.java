/*  
Requirements:
    Create a program that reads an unspecified number of integer arguments from the command line and adds them together.
    For example, suppose that you enter the following: java Adder 1 3 2 10
    The program should display 16 and then exit. The program should display an error message if the user enters only one argument.
    
Entity:
    SimpleAdder
    
Function decalartion: 
    public static void main(String[] args) {}
    public void add(int... array);

Jobs To Be Done:
	1)Create an object for the class SimpleAdder as adder.
    2)Invoke the method add
    	2.1)Check whether the length of the array is 1
           	2.1.1)Print as Add more numbers.
       	2.2)Otherwise decalre the variable sum as int and store 0 in it .
           	2.2.1)for each number in the array
                 2.2.1.1)Add sum and number and store in sum.
       	2.3)Print the sum
    3)Invoke the method add so that the procedure from 2.1 t0 2.3 will be executed. 
 */

package com.java.training.exercise2.datatypes;

public class AdderDemo {

	public void add(int... array) {

		if(array.length == 1) {

			System.out.println("Add more numbers");

		} else {

			int sum = 0;
			for(int number : array) {

				sum += number;

			}
			System.out.println(sum);
		}
	}

	public static void main(String[] args) {

		AdderDemo adder = new AdderDemo();
		adder.add(1,3,2,10);
		adder.add(1);
	}
}	
