/*
Requirement:
    To invert the value of a boolean, which operator would you use?

Entity:
    BooleanInvert

Function Declaration:
    public static void main(String[] args) {}

Jobs To Be Done:
    1) Declare the variable value as boolean and assingn "true".
    2) Print the invert value.
 */

package com.java.training.exercise2.datatypes;

public class InvertValue {

	public static void main(String[] args) {

		boolean value = true;
		System.out.println(!value);
	}
}
