/*
Requirement:
    Requirement:To demonstrate overloading with Wrapper types

Entity:
    Wrapper

Function Declaration:
    intWrap(int value);
    charWrap(char value);
    doubleWrap(double value);
    public static void main(String[] args) {}

Jobs To Be Done:
    1) Declare the variable integer as type Integer and store 117.
    2) Declare the variable character as type Character and store "G".
    3) Create an instance for WrapperOverloading.
    4) Invoke the method intWrapper and pass the parameter integer.
    5) Invoke the method charWrapper and pass the parameter character.
 */

package com.java.training.exercise2.datatypes;

public class WrapperOverLoading {

	public void intWrapper(int value) {

		System.out.println("int is converted to Integer : " + value);
	}

	public void charWrapper(char value) {

		System.out.println("char is converted to Character : " + value);
	}

	public static void main(String[] args) {

		Integer integer = 100;
		Character character = 'T';

		WrapperOverLoading wrapper = new WrapperOverLoading();

		wrapper.intWrapper(integer);
		wrapper.charWrapper(character);
	}
}