/*Data type exercise:
==================
+ print the type of the result value of following expressions
  - 100 / 24    // 4
  - 100.10 / 10 // 10.01
  - 'Z' / 2     // 45
  - 10.5 / 0.5  // 21.0
  - 12.4 % 5.5  // 1.4000000000000004
  - 100 % 56    // 44


+ What is the value of the following expression, and why?
     Integer.valueOf(1).equals(Long.valueOf(1))
ANSWER:-
    Both values differ in their datatype. So when their values are comopared, it gives false value.
*/