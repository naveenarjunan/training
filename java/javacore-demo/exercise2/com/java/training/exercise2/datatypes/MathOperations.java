/*
Requirement: print the type of the result value of following expressions
            - 100 / 24
            - 100.10 / 10
            - 'Z' / 2
            - 10.5 / 0.5
            - 12.4 % 5.5
            - 100 % 56

Entity:
    MathOperations

Function Declaration:
    public static void main(String[] args)

Jobs To Be Done:
    1) Create an object for every operation.
    2) Print the type of the every operation's result.
 */

package com.java.training.exercise2.datatypes;

public class MathOperations {

	public static void main(String[] args) {

		Object one = 100 / 24;
		System.out.println(one.getClass().getName());

		Object two = 100.10 / 10;
		System.out.println(two.getClass().getName());

		Object three = 'Z' /2;
		System.out.println(three.getClass().getName());

		Object four =10.5 /0.5 ;
		System.out.println(four.getClass().getName());

		Object five = 12.4 % 5.5;
		System.out.println(five.getClass().getName());

		Object six = 100/56;
		System.out.println(six.getClass().getName());
	}
}