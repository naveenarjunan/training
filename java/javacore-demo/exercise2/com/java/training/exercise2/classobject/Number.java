package com.java.training.exercise2.classobject;

/*Requirement:
    To find the output of the given program if the value of aNumber is 3.
        if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
        System.out.println("third string");

Entities:
    It doesn't have any class name

Function Declaration:
    There is no function.

Jobs To Be Done:
    1)Checking the aNumber value greater than or equal to zero.
    2)It is true, So it moves to the next statement.
    3)Checking the aNumber value equal to zero 
    4)It is false, So it skips the step and move to the else part.
    5)There is no else part in these if type. So it moves to the else of previous if part.
    6)It prints the second string and followed by third string.

/* if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string"); 
        System.out.println("third string");
*/
/*
O/P:
second string
third string
*/