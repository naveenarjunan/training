/*Requirement:
    * To write code that creates an instance of the class 
    * initializes its two member variables with provided values
    * then displays the value of each member variable.

Entities:
    class:-  "NumberHolder"

Function Declaration:
    There is no function 

Jobs To Be Done:
    1)Create an instance of the class 
    2)Create the object numberHolder 
    3)Initialize the value for the variables
    4)Display the instance variable values

*/

package com.java.training.exercise2.classobject;

public class NumberHolder {
	public int anInt;
	public float aFloat;
	public static void main(String[] args) {
		NumberHolder numberHolder = new NumberHolder();
		numberHolder.anInt = 1;
		numberHolder.aFloat = 2.3f;
		System.out.println(numberHolder.anInt);   //1
		System.out.println(numberHolder.aFloat);   //2.3
	}
}