/*Requirement:
    To correct the error for the given program
	
Entities:
    class:-  "SomethingIsWrong"

Function Declaration:
    There is a function called area()

Jobs To Be Done:
    1)Find the error in the program.
    2)Change the class name from SomethingIsWrong to Rectangle.
    3)Declaring the property with int type and also declaring the object
    4)Create the method area() and return the product of width and height.
*/

package com.java.training.exercise2.classobject;

public class Rectangle {
	public int width;
	public int height;
	public  int area(){
		return (width * height);
	}

	public static void main(String[] args) {
		Rectangle myRect = new Rectangle();
		myRect.width = 40;
		myRect.height = 50;
		System.out.println("myRect's area is " + myRect.area());
	}
}