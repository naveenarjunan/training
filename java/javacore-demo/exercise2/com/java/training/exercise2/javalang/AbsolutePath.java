/*
Requirement:
    print the absolute path of the .class file of the current class
    
Entity:
    AbsolutePath
    
Function Declaration:
    public static void main(final String[] args) {}
    
Jobs To Be Done:
    1) Print the absolute path of the file.
*/

package com.java.training.exercise2.javalang;

public class AbsolutePath {

	public static void main(String[] args) {

		System.out.println("current path : " +System.getProperty("user.dir" ));
	}
}