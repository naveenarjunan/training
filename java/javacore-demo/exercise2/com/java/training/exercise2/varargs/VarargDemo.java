/*
Requirement:
    To demonstrate overloading with varArgs

Entity:
    VarargDemo ,VariableArgument

Function declaration: 
   public static void main(String[] args)
   public void printNumbers(int... number)

Jobs to be done:
    1)An object is cretaed for class VaraiableArgument as variableArgument.
    2)Invoke printNumbers method
      2.1)For each number
          2.1.1)print the number.
    3)Invoke printNumbers method     
      3.1)For each number
          3.1.1)print the number.       
*/


package com.java.training.exercise2.varargs;

class VariableArgument{

	public void printNumbers(int... number) {

		System.out.println("The numbers are:");
		for(int i : number) {
			System.out.println(i);
		}
	}  
}

public class VarargDemo{

	public static void main(String[] args) {

		VariableArgument variableArgument = new VariableArgument();
		variableArgument.printNumbers(1,2,3);
		variableArgument.printNumbers(1,2,3,4,5,6,7,8,9,10);  
	}
}