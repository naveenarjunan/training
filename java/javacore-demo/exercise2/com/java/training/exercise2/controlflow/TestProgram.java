/*
    To find the output of the given program if the value of aNumber is 3.
        if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
        System.out.println("third string");

 */

package com.java.training.exercise2.controlflow;

class Test {

	public static void main (String args[]) {

		int aNumber = 3;
		if (aNumber >= 0) {

			if (aNumber == 0) {

				System.out.println("first string");
			}
		} else {

			System.out.println("second string");
		}
		System.out.println("third string");
	}
}

/*OUTPUT:
third string
 */