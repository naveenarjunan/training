/*
Requirement:
    To print the Fibonacci series using while loop.
    
Entity:
    class FibonacciWhile
    
Function Declaration:
    public static void main(String args[]) {}
    
Jobs to be Done:
    1) Declare and initialize the variable limit, initialValue, nextValue, sum, i.
    2) Check if i less than or equal to range
    	2.1) Add the initialValue and nextValue and store it in sum.
    	2.2) Swap the values
    	    2.2.1) nextValue to the initialValue.
    		2.2.2) sum to the nextValue.
    		2.2.3) increment the value i.
    	2.3) Print the initialValue
*/

package com.java.training.exercise2.controlflow;

class FibonacciWhile {

	public static void main(String args[]) {

		int sum;
		int initialValue = 0;
		int nextValue = 1;
		int i = 1;
		int range = 10;

		while(i <= range) {

			sum = initialValue + nextValue;
			initialValue = nextValue;
			nextValue = sum;
			i++;
			System.out.println(initialValue);
		}
	}
}
