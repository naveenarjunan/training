/*
Requirement:
    To print the Fibonacci series using recursion.

Entity:
    class FibonacciRecursion

Function Declaration:
    printFibonacciRecursion(n);
    public static void main(String[] args) {}

Jobs to be Done:
    1) Initialize the static variables initialValue, nextValue, sum.
    2) Create a method printFibonacciRecursion(int range)
     	2.1) check the range is greater than zero.
    	2.2) Add the initialValue and nextValue and store it in sum.
    	2.3) Swap the values
    	    2.3.1) nextValue to the initialValue.
    		2.3.2) sum to the nextValue.
    	2.4) Print the initialValue
    	2.5) Reduce the range by 1 in the method(printFibonacciRecursion(range - 1))
    3) Initialize the variable range and set the value 10.
    4) Create an object for FibonacciRecursion.
    5) Invoke the method printFibonacciRecursion(range).

 */

package com.java.training.exercise2.controlflow;

public class FibonacciRecursion {

	public int intialValue = 0;
	public int nextValue = 1;
	public int sum;

	public void printFibonacciRecursion(int range) {

		if(range > 0) {

			sum = intialValue + nextValue;
			intialValue = nextValue;
			nextValue = sum;
			System.out.println(intialValue);
			printFibonacciRecursion(range - 1);
		}
	}

	public static void main(String[] args) {

		int range = 10;
		FibonacciRecursion recursion = new FibonacciRecursion();
		recursion.printFibonacciRecursion(range);        
	}
}
