/*
Requirement:
    To print the Fibonacci series using for loop.
    
Entity:
    class FibonacciFor
    
Function Declaration:
    public static void main(String args[]) {}
    
Jobs To Be Done:
    1) Declare and initialize the variable limit, initialValue, nextValue, sum, i.
    2) In for loop initialize i, set the limit and increment the value i
    	2.1) Add the initialValue and nextValue and store it in the sum.
    	2.2) Swap the values 
    		2.2.1) store the nextValue to the initialValue
    		2.2.2) store the sum to the nextValue
    	2.3) Print the initial values of the loop.
    3) Print the result. 
*/

package com.java.training.exercise2.controlflow;

public class FibonacciFor {

	public static void main(String args[]) {

		int limit = 10;
		int initialValue = 0;
		int nextValue = 1;
		int sum,i;

		System.out.println("Fibonacci Series:");

		for(i=1; i<=limit; i++) {

			sum= initialValue + nextValue;
			initialValue = nextValue;
			nextValue = sum;
			System.out.println(initialValue);
		}
	}
}
