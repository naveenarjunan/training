/*Requirement:
    * In the given program, called ComputeResult, what is the value of result after each numbered line executes?


Entities:
    class:-  "ComputeResult"

Function Declaration:
     public static void main(String[] args)

Jobs To Be Done:
    1)execute the given program
	2)find the value of result after each numbered line executes?

*/

package com.java.training.exercise2.strings;

public class ComputeResult {

	public static void main(String[] args) {

		String original = "software";
		StringBuilder result = new StringBuilder("hi");
		int index = original.indexOf('a');

		result.setCharAt(0, original.charAt(0));                          /*1*/
		result.setCharAt(1, original.charAt(original.length()-1));        /*2*/
		result.insert(1, original.charAt(4));                             /*3*/
		result.append(original.substring(1,4));                           /*4*/
		result.insert(3, (original.substring(index, index+2) + " "));     /*5*/

		System.out.println(result);
	}
}
/*
Answer:

si
se
swe
sweoft
swear oft
 */