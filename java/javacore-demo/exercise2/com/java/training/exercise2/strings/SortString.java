/*Requirement:
    * sort and print following String[] alphabetically ignoring case.
    * convert and print even indexed Strings into uppercase
    * { "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" }

Entities:
    class:-  "SortString"

Function Declaration:
    public static void main(String[] args)

Jobs To Be Done:
    1)Create an String array named array and store the given cities.
    2)Sort the array ignoring cases.
    3)print the sorted array.
    4)For each index of the array
      4.1)Check whether the index is even
          4.1.1)If the condition is true then change the element at the particular index to upper
          case.
    5)Print the array.

*/

package com.java.training.exercise2.strings;

import java.util.Arrays;

class SortString{
    
    public static void main(String[] args) {      
        String[] array={ "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" };
        Arrays.sort(array,String.CASE_INSENSITIVE_ORDER);
        System.out.println("sorted list:");
        System.out.println(Arrays.asList(array));
        for(int i=0; i<array.length; i++){
           
            if(i%2==0){
               
               String c=array[i];
               String b= c.toUpperCase();
               array[i]=b;
            }
        } 
        System.out.println("even index in uppercase:");
        System.out.println(Arrays.asList(array));
    }
}