/*Requirement:
    *Write a program that computes your initials from your full name.
    *displays them.

Entities:
    class:-  "Initials"

Function Declaration:
    There is no function 

Jobs To Be Done:
    1)create a class called Initials.
	2)to Write a program that computes your initials from your full name.
	3)to displays them.


*/
package com.java.training.exercise2.strings;

public class Initials {

	public static void main(String[] args) {

		String name = "Naveen Arjunan";
		StringBuffer myInitials = new StringBuffer();
		int length = name.length();

		for (int i = 0; i < length; i++) {
			if (Character.isUpperCase(name.charAt(i))) {
				myInitials.append(name.charAt(i));
			}
		}
		System.out.println("My initials are: " + myInitials);
	}
}