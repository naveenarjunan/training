/*Requirement:
 *  explain why the value "6" is printed twice in a row:
       class PrePostDemo {
           public static void main(String[] args){
               int i = 3;
               i++;
               System.out.println(i);    // "4"
               ++i;
               System.out.println(i);    // "5"
               System.out.println(++i);  // "6"
               System.out.println(i++);  // "6"
               System.out.println(i);    // "7"
           }
       }

Entities:
    class:-  "PrePostDemo"

Function Declaration:
    public static void main(String[] args)

Jobs To Be Done:
    1)to find why the number "6" is printed twice in the program

 */

package com.java.training.exercise2.operators;

class PrePostDemo {
	public static void main(String[] args){
		int i = 3;
		i++;
		System.out.println(i);    // "4"
		++i;
		System.out.println(i);    // "5"
		System.out.println(++i);  // "6"
		System.out.println(i++);  // "6"
		System.out.println(i);    // "7"
	}
}

/*
   1)in this "System.out.println(++i)",the ++i is a pre-increament operator.so the value "5" is increamented to "6" .
   2)in this "System.out.println(i++)",the i++ is a post-increament operator.so the value "6" is displayed as it is as "6".
   3)in the next case ,the "6" is increamented to "7"
 */