package com.sports.training.core.field;

public class Coach {
    
    public String name;
    protected int id;
    protected int age;
    public String gender;
    
    public Coach() {        // unparameterized constructor
        
        name = "HEAD";
        id = 1;
        age = 50;
    }
    
    public Coach(String a) {
        
        name = a;
    }
    
    public Coach(String a,int b) {
        
        name = a;
        id = b;
    }
    
    public Coach(String name,int id,int age) {          // parameterized constructor
        
        this.name = name;
        this.id = id;
        this.age = age;
    }
    protected void prints() {       
        
        System.out.println("Name :" + name + "," + "Age :" + age + "," + "Gender :" + gender);
        System.out.println("CoachID :" + id);
    }
    
    public static void main(String args[]) {
        
        Coach coach = new Coach();
        Coach coach1 = new Coach("naveen");
        Coach coach2 = new Coach("arjun",3);
        coach.prints();
        coach1.prints();
        coach2.prints();
    }
}
