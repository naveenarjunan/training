package com.sports.training.core.coach;

import com.sports.training.core.field.Coach;


class Player extends Coach {
    
    public int id;
    protected String type = "player";
    private String game = "football";
    
    public Player() {
        
        id = 0;
    }
        
    public Player(int a,String b,String c) {
        
        id = a;
        type = b;
        game = c;
    }
    
    public void print() {
        
        System.out.println("ID :" + id +"," + "Type :" + type + "," + "game :" + game);

    }
        
    public static void main(String args[]) {
        
        Player player = new Player();
        Player player1 = new Player(1,"player","basket ball");
        player.prints();
        player.print();
        player1.print();
    }
}
   