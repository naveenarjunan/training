package in.kpr.training.jdbc.exception;                                                                                       
                                                                                                                               
public enum ErrorCode {                                                                                                        
                                                                                                                               
    POSTAL_CODE_ZERO("ERR401", "postal code should not be zero"),          
    READING_ADDRESS_FAILED("ERR402", "Failed to read address"),             
    ADDRESS_CREATION_FAILED("ERR403", "Address was not created"),           
    ADDRESS_UPDATION_FAILED("ERR404", "Failed to update Address"),          
    ADDRESS_DELETION_FAILED("ERR405", "Failed to delete Address"),          
    PERSON_CREATION_FAILED("ERR406", "Person was not created"),             
    PERSON_UPDATION_FAILED("ERR407", "Failed to update Person"),            
    PERSON_DELETION_FAILED("ERR408", "Failed to delete Person"),            
    CONNECTION_FAILS("ERR409", "Failed to initiate connection"),           
    EMAIL_NOT_UNIQUE("ERR411", "Email should be unique"),                  
    CONNECTION_FAILS_TO_CLOSE("ERR412", "Failed to close the connection"), 
    FAILED_TO_COMMIT("ERR413", "Failed to commit"),                        
    FAILED_TO_ROLLBACK("ERR414", "Failed to rollback"),                    
    FAILED_TO_CHECK_EMAIL("ERR415", "Failed to check email"),              
    READING_PERSON_FAILED("ERR416", "Failed to read person"),               
    READING_ADDRESSID_FAILED("ERR417", "Failed to read address id"),        
    FAILED_TO_CHECK_ADDRESS("ERR418", "Failed to check address"),          
    SEARCHING_ADDRESS_FAILED("ERR419", "Failed to search address"),         
    FAILED_TO_GET_ADDRESS_SIZE("ERR420", "Failed to get size of address table"),                
    FIRST_NAME_AND_LAST_NAME_DUPLICATE("ERR421", "First name and last name should be unique"),  
    CHECKING_ADDRESS_USAGE_FAILED("ERR422", "Failed to check address usage"),                    
    WRONG_DATE_FORMAT("ERR423", "Enter valid date in valid format dd-mm-yyyy"),           
    CHECKING_UNIQUE_NAME_FAILED("ERR424", "Failed to check name is unique or not"),        
    SETTING_VALUE_FAILED("ERR425", "Failed to set value for Prepared Statement"),   
    STREET_NAME_ERROR("ERR426", "street should not be not null and non-empty"),    
    CITY_NAME_ERROR("ERR427", "city should not be not null and non-empty"), 
    FIRST_NAME_ERROR("ERR428", "First name should not be null or empty"),   
    LAST_NAME_ERROR("ERR429", "Last name should not be null or empty"),     
    EMAIL_ERROR("ERR430", "Email should not be null or empty"); 
                                                                
    private final String code;               
    private final String message;            
                                             
    ErrorCode(String code, String message) { 
        this.code = code;                    
        this.message = message;              
    }                                        
                                             
    public String getCode() {                
        return this.code;                    
    }                                        
                                             
    public String getMessage() {             
        return this.message;                 
    }                                        
}