package in.kpr.training.jdbc.constants;

public class Constant {

	public static final String URL            = "url";
    public static final String USER_NAME      = "user";
    public static final String PASSWORD       = "password";
    public static final String ID             = "id";
    public static final String STREET         = "street";
    public static final String CITY           = "city";
    public static final String POSTAL_CODE    = "postal_code";
    public static final String NAME           = "name";
    public static final String EMAIL          = "email";
    public static final String ADDRESS_ID     = "address_id";
    public static final String BIRTH_DATE     = "birth_date";
    public static final String CREATED_DATE   = "created_date";
	public static final int MAX_POOL_SIZE	  = 3;
	public static final String GENERATED_KEYS = "GENERATED_KEYS" ; 
	public static final String FIRST_NAME     = "first name";
	public static final String LAST_NAME      = "last name";
}