package in.kpr.training.jdbc.constants;

public class QueryStatements {

    public static final String CREATE_ADDRESS_QUERY     = new StringBuilder()
            .append("INSERT INTO details.address (street, city, postal_code)")
            .append("     VALUES (?, ?, ?)                          ")
            .toString();

    public static final String READ_ADDRESS_QUERY       = new StringBuilder()
            .append("SELECT address.id         ")
            .append("      ,address.street     ")
            .append("      ,address.city       ")
            .append("      ,address.postal_code")
            .append("  FROM details.address            ")
            .append(" WHERE id = ?             ")
            .toString();

    public static final String READALL_ADDRESS_QUERY    = new StringBuilder()
            .append("SELECT address.id         ")
            .append("      ,address.street     ")
            .append("      ,address.city       ")
            .append("      ,address.postal_code")
            .append("  FROM details.address    ")
            .toString();
    
    public static final String UPDATE_ADDRESS_QUERY     = new StringBuilder()
            .append("UPDATE details.address         ")
            .append("   SET street = ?      ")
            .append("      ,city = ?        ")
            .append("      ,postal_code = ? ")
            .append(" WHERE id = ?          ")
            .toString();
    
    public static final String DELETE_ADDRESS_QUERY     = new StringBuilder()
            .append("DELETE FROM details.address")
            .append(" WHERE id=?        ")
            .toString();

    public static final String CREATE_PERSON_QUERY      = new StringBuilder()
            .append("INSERT INTO details.person (first_name, last_name, email, birth_date, address_id)")
    		.append("     VALUES (?, ?, ?, ?, ?)                                              ")
    		.toString();

    public static final String READ_PERSON_QUERY        = new StringBuilder()
            .append("SELECT id           ")// remove person
            .append("      ,first_name   ")
            .append("      ,last_name    ")
            .append("      ,email        ")
            .append("      ,address_id   ")
            .append("      ,birth_date   ")
            .append("      ,created_date ")
            .append(" FROM details.person               ")
            .append("WHERE id = ?               ")
            .toString();

    public static final String READALL_PERSON_QUERY     = new StringBuilder()
            .append("SELECT id           ")
            .append("      ,first_name   ")
            .append("      ,last_name    ")
            .append("      ,email        ")
            .append("      ,address_id   ")
            .append("      ,birth_date   ")
            .append("      ,created_date ")
            .append("  FROM details.person              ")
            .toString();

    public static final String UPDATE_PERSON_QUERY      = new StringBuilder()
            .append("UPDATE details.person        ")
            .append("   SET first_name = ?")
            .append("      ,last_name = ? ")
            .append("      ,email = ?     ")
            .append("      ,address_id = ?")
            .append("      ,birth_date = ?")
            .append(" WHERE id = ?        ")
            .toString();
    
    public static final String DELETE_PERSON_QUERY      = new StringBuilder()
            .append("DELETE FROM details.person")
            .append(" WHERE id=?       ")
            .toString();

    public static final String EMAIL_UNIQUE             = new StringBuilder()
            .append("SELECT id       ")
            .append("  FROM details.person          ")
            .append(" WHERE email = ?")
            .toString();
    
    public static final String GET_ADDRESS_ID           = new StringBuilder()
            .append("SELECT address_id ")
            .append("  FROM details.person            ")
            .append(" WHERE id = ?     ")
            .toString();
    
    public static final String ADDRESS_UNIQUE           = new StringBuilder()
            .append("SELECT id                 ")
            .append("  FROM details.address                    ")
            .append(" WHERE street = ?         ")
            .append("       AND city = ?       ")
            .append("       AND postal_code = ?")
            .toString();
    
    public static final String ADDRESS_SEARCH           = new StringBuilder()
            .append("SELECT id         ")
            .append("      ,street     ")
            .append("      ,city       ")
            .append("      ,postal_code")
            .append("  FROM details.address            ")
            .append(" WHERE street             ")
            .append("       LIKE ?             ")
            .append("       AND city           ")
            .append("       LIKE ?             ")
            .append("       AND postal_code    ")
            .append("       LIKE ?             ")
            .toString();
    
    public static final String ADDRESS_TABLE_SIZE       = new StringBuilder()
            .append("SELECT COUNT(*) ")
            .append("  FROM details.address  ")
            .toString();

    public static final String PERSON_TABLE_SIZE        = new StringBuilder()
            .append("SELECT COUNT(*) ")
            .append("  FROM details.person   ")
            .toString();
    
    public static final String ADDRESS_USAGE            = new StringBuilder()
            .append("SELECT COUNT(person.id)     ")
            .append("  FROM details.person               ")
            .append(" WHERE address_id = ?")
            .toString();
    
    public static final String NAME_UNIQUE              = new StringBuilder()
            .append("SELECT id               ")
            .append("  FROM details.person                  ")
            .append(" WHERE first_name = ?   ")
            .append("       AND last_name = ?")
            .toString();
}