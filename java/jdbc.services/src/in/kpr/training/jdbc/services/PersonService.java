/*
Requirement:
    To perform the CRUD operation of the Person.
 
Entity:
    1.Person
    2.PersonService
    3.AppException
    4.ErrorCode
 
Function declaration:
    public long create(Person person, Address address)
    public Person read(long id, boolean addressFlag)
    public ArrayList<Person> readAll()
    public void update(long id, Person person, Address address)
    public void delete(long id)

Jobs To Be Done:
    1. Create a Person.
    2. Read a record in the Person.
    3. Read all the record in the Person.
    4. Update a Person.
    5. Delete a Person.
*/

package in.kpr.training.jdbc.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import in.kpr.training.jdbc.constants.Constant;
import in.kpr.training.jdbc.constants.QueryStatements;
import in.kpr.training.jdbc.exception.AppException;
import in.kpr.training.jdbc.exception.ErrorCode;
import in.kpr.training.jdbc.model.Person;

public class PersonService {

    private long personId;
    private long addressId;
    private ResultSet resultSet;
    
    public long create(Person person) {

        validatePerson(person);
        checkNameIsUnique(0, person, ConnectionService.get());
        checkEmailIsUnique(0, person.getEmail(), ConnectionService.get());
        
        try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatements.CREATE_PERSON_QUERY,
                PreparedStatement.RETURN_GENERATED_KEYS)) {
            
            addressId = assignAddressId(person);
            
            setPerson(ps, person);
            
            if (ps.executeUpdate() != 1 || !((resultSet = ps.getGeneratedKeys()).next())) {
                throw new AppException(ErrorCode.PERSON_CREATION_FAILED);
            }
            
            return resultSet.getLong("GENERATED_KEY");
        } catch (Exception e) {
            throw new AppException(ErrorCode.PERSON_CREATION_FAILED, e);
        }
    }

    public Person read(long id, boolean addressFlag) {

        Person person = null;
        AddressService addressService = new AddressService();

        try (PreparedStatement ps =
                ConnectionService.get().prepareStatement(QueryStatements.READ_PERSON_QUERY)) {

            ps.setLong(1, id);
            
            if ((resultSet = ps.executeQuery()).next()) {
                person = readPerson(resultSet);
            }
            if (addressFlag) {
                person.setAddress(addressService.read(resultSet.getLong(Constant.ADDRESS_ID)));
            }

            return person;
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_PERSON_FAILED, e);
        }
    }

    public ArrayList<Person> readAll() {

        ArrayList<Person> persons = new ArrayList<>();
        AddressService addressService = new AddressService();
        Person person;

        try (PreparedStatement ps =
                ConnectionService.get().prepareStatement(QueryStatements.READALL_PERSON_QUERY)) {

            resultSet = ps.executeQuery();

            while (resultSet.next()) {
                person = readPerson(resultSet);
                person.setAddress(addressService.read(resultSet.getLong(Constant.ADDRESS_ID)));
                persons.add(person);
            }

            return persons;
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_PERSON_FAILED, e);
        }
    }

    public void update(Person person) {
        
        checkNameIsUnique(person.getId(), person, ConnectionService.get());
        checkEmailIsUnique(person.getId(), person.getEmail(), ConnectionService.get());
        
        try (PreparedStatement ps = ConnectionService.get()
                .prepareStatement(QueryStatements.UPDATE_PERSON_QUERY.toString())) {

            addressId = assignAddressId(person);
            setPerson(ps, person);
            ps.setLong(6, person.getId());

            if (ps.executeUpdate() == 0) {
                throw new AppException(ErrorCode.PERSON_UPDATION_FAILED);
            }
        } catch (Exception e) {
            throw new AppException(ErrorCode.PERSON_UPDATION_FAILED, e);
        }

    }

    public void delete(long id) {

        if (getAddressIdByPersonId(id, ConnectionService.get()) != 0) {

                AddressService addressService = new AddressService();
                addressService.delete(getAddressIdByPersonId(id, ConnectionService.get()));
            
        }

        try (PreparedStatement ps =
                ConnectionService.get().prepareStatement(QueryStatements.DELETE_PERSON_QUERY)) {
            ps.setLong(1, id);

            if (ps.executeUpdate() == 0) {
                throw new AppException(ErrorCode.PERSON_DELETION_FAILED);
            }
        } catch (Exception e) {
            throw new AppException(ErrorCode.PERSON_DELETION_FAILED, e);
        }
    }


    public void checkEmailIsUnique(long id, String email, Connection con) {

        ResultSet resultSet;
        boolean unique = true;

        try (PreparedStatement ps = con.prepareStatement(QueryStatements.EMAIL_UNIQUE)) {
            
            ps.setString(1, email);
            
            if ((resultSet = ps.executeQuery()).next()) {
                personId = resultSet.getLong(Constant.ID);
            }
            
            if (id == 0) {
                unique = personId > 0 ? false : true;
            } else {
                unique = (personId > 0 && personId != id) ? false : true;
            }

        } catch (Exception e) {
            throw new AppException(ErrorCode.FAILED_TO_CHECK_EMAIL, e);
        }

        if (unique == false) {
            throw new AppException(ErrorCode.EMAIL_NOT_UNIQUE);
        }
    }
    
    public void checkNameIsUnique(long id, Person person, Connection con) {

        boolean unique = true;

        try (PreparedStatement ps = con.prepareStatement(QueryStatements.NAME_UNIQUE)) {
            
            ps.setString(1, person.getFirstName());
            ps.setString(2, person.getLastName());

            if ((resultSet = ps.executeQuery()).next()) {
                personId = resultSet.getLong(Constant.ID);
            }

            if (id == 0) {
                unique = personId > 0 ? false : true;
            } else {
                unique = (personId > 0 && personId != id) ? false : true;
            }
        } catch (Exception e) {
            throw new AppException(ErrorCode.CHECKING_UNIQUE_NAME_FAILED, e);
        }

        if (unique == false) {
            throw new AppException(ErrorCode.FIRST_NAME_AND_LAST_NAME_DUPLICATE);
        }
    }

    public long getAddressIdByPersonId(long id, Connection con) {
        
        try (PreparedStatement ps = con.prepareStatement(QueryStatements.GET_ADDRESS_ID)) {

            ps.setLong(1, id);
            if (!(resultSet = ps.executeQuery()).next()) {
                addressId = 0;
            }
            addressId = resultSet.getLong(Constant.ADDRESS_ID);
            
            return addressId;
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_ADDRESSID_FAILED, e);
        }
    }
    
    public long assignAddressId(Person person) {
        
        AddressService addressService = new AddressService();

        if (person.getAddress() != null) {

            if (addressService.getAddressId(person.getAddress(),
                    ConnectionService.get()) > 0) {
                addressId = addressService.getAddressId(person.getAddress(),
                        ConnectionService.get());
            } else {
                addressId = addressService.create(person.getAddress());
            }
        }

        return addressId;
    }

    public Person readPerson(ResultSet resultSet) {

        Person person = null;

        try {
            person = new Person(resultSet.getString(Constant.FIRST_NAME),
                    resultSet.getString(Constant.LAST_NAME), resultSet.getString(Constant.EMAIL),
                    new java.util.Date(resultSet.getDate(Constant.BIRTH_DATE).getTime()));
            person.setCreatedDate(resultSet.getTimestamp(Constant.CREATED_DATE));
            person.setId(resultSet.getLong(Constant.ID));

            return person;
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_PERSON_FAILED, e);
        }
    }
    
    public void setPerson(PreparedStatement ps, Person person) {
        
        try {
            
            ps.setString(1, person.getFirstName());
            ps.setString(2, person.getLastName());
            ps.setString(3, person.getEmail());
            ps.setDate(4, new java.sql.Date(person.getBirthDate().getTime()));
            ps.setLong(5, addressId);
        } catch (Exception e) {
            throw new AppException(ErrorCode.SETTING_VALUE_FAILED, e);
        }
    }

    public static Date dateValidator(String date) {
        
        try {
            
            Date utilDate = new SimpleDateFormat("dd-MM-yyyy").parse(date);
            return utilDate;
            
        } catch (Exception e) {
            throw new AppException(ErrorCode.WRONG_DATE_FORMAT, e);
        }
    }
    
    public void validatePerson(Person person) {
        
        if (person.getFirstName() == null) {
            throw new AppException(ErrorCode.FIRST_NAME_ERROR);
        }
        
        if (person.getLastName() == null) {
            throw new AppException(ErrorCode.LAST_NAME_ERROR);
        }
        
        if (person.getEmail() == null) {
            throw new AppException(ErrorCode.EMAIL_ERROR);
        }
    }
}
