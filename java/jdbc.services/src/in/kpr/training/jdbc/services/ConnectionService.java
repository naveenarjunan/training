/*
Requirement:
    To create a connection between the database and java application.
    
Entity:
    JdbcConnection
    
Function declaration:
    public void init() 
    public void release() 
    
Jobs to be done:
    
    1. Declare Connection con.
    2. Declare init method.
    	2.1 Load db.properties file to a properties object.
        2.2 Establish the connection using sql driver and store it as con.
    3. Declare get method.
        3.1 Return connection object.
    4. Declare release method.
        4.1 Close the Connection of the server.
        4.2 Close the PreparedStatement of the query.
    5. Declare commit method.
        5.1 Commit the changes made after execution of query.
    6. Declare rollback method.
        6.1 Rollback the changes made by executing the query.

Pseudo code:
class ConnectionService {
    
    public Connection con;

    public void init() {

        Properties properties = new Properties();
        
        try {
            InputStream resourceAsStream =
                    ConnectionService.class.getClassLoader().getResourceAsStream("db.properties");
            if (resourceAsStream != null) {
                properties.load(resourceAsStream);
            }
            
            con = DriverManager.getConnection(properties.getProperty("URL"),
                    properties.getProperty("User Name"), properties.getProperty("Password"));
            con.setAutoCommit(false);

        } catch (Exception e) {
            throw new AppException(ErrorCode.CONNECTION_FAILS, e);
        } 

    }
    
    public static Connection get() {
        
        return con;
    }

    public void release() {
        try {
            con.close();
        } catch (SQLException e) {
            throw new AppException(ErrorCode.SQLException);
        }
    }
    
    public void commit() {
        try {
            con.commit();
        } catch (SQLException e) {
            throw new AppException(ErrorCode.SQLException);
        }
    }

    public void rollback() {
        try {
            con.rollback();
        } catch (SQLException e) {
            throw new AppException(ErrorCode.SQLException);
        }
    }
}

*/

package in.kpr.training.jdbc.services;

import java.io.InputStream;
import java.sql.Connection;
import java.util.Properties;

import javax.sql.DataSource;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import in.kpr.training.jdbc.constants.Constant;
import in.kpr.training.jdbc.exception.AppException;
import in.kpr.training.jdbc.exception.ErrorCode;

public class ConnectionService {
    
    private static ThreadLocal<Connection> threadLocal = new ThreadLocal<>();
    private static DataSource datasource;
    
    public static DataSource getDataSource() {
        
        if(datasource == null) {
            
            HikariConfig config = new HikariConfig();
            Properties properties = new Properties();

            try {
                InputStream resourceAsStream =
                        ConnectionService.class.getClassLoader().getResourceAsStream("db.properties");

                if (resourceAsStream != null) {
                    properties.load(resourceAsStream);
                }
                
                config.setJdbcUrl(properties.getProperty(Constant.URL));
                config.setUsername(properties.getProperty(Constant.USER_NAME));
                config.setPassword(properties.getProperty(Constant.PASSWORD));
                config.setMaximumPoolSize(Constant.MAX_POOL_SIZE);
                config.setAutoCommit(false);

                datasource = new HikariDataSource(config);
                
            } catch (Exception e) {
                throw new AppException(ErrorCode.CONNECTION_FAILS, e);
            }
        }
        return datasource;
    }
    
    public static void init() {
        
        try {
            threadLocal.set(getDataSource().getConnection());
        } catch (Exception e) {
            throw new AppException(ErrorCode.CONNECTION_FAILS_TO_CLOSE, e);
        }
    }
    
    public static Connection get() {
        
        if (threadLocal.get() == null) {
            init();
        }
        return threadLocal.get();
    }

    public static void commit(boolean b) {
        
        try {
            
            if (b == true) {
                threadLocal.get().commit();
            } else {
                threadLocal.get().rollback();
            }
            
        } catch (Exception e) {
            throw new AppException(ErrorCode.FAILED_TO_COMMIT);
        }
    }
    
    public static void release() {
        
        try {
            threadLocal.get().close();
            threadLocal.remove();
        } catch (Exception e) {
            throw new AppException(ErrorCode.CONNECTION_FAILS_TO_CLOSE, e);
        }
    }
}