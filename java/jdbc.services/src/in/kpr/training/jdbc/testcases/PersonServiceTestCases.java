/*
Requirement:
    To test the conditions in the PersonService.

Entity:
    1.PersonServiceTestCase
    2.AddressService
    3.AppExcetion
    4.Person
    5.Address

Function declaration:
    public void setup() {}
    public void personCreationTest() {}
    public void personCreationTest1() {}
    public void personCreationTest2() throws AppException {}
    public void personCreationTest3() throws AppException {}
    public void personReadTest() {}
    public void personReadTest2() {}
    public void personReadTest1() {}
    public void personReadAllTest() {}
    public void personUpdationTest() {}
    public void personUpdationTest1() {}
    public void personUpdationTest2() {}
    public void personUpdationTest3() {}
    public void personDeleteTest() {}
    public void personDeleteTest1() {}

Jobs to be done:
    1. Create an instance of PersonService as personService in private.
        1.1 Declare address, address1,address2 of type Address in private.
        1.2 Declare person, person1, person2, person3, person4 of type Person in private.
        1.3 Declare id of type long in private.
    2. Inside a setup() method
        2.1 Set the respective values for the declared variables by creating objects of Person and Address.
    3. Priority one is to check the address creation of the person with postal_code as zero.
        3.1 Invoke the create method of personService
        3.2 Check whether it throws the expected AppException.
    4. Priority two is to check the address creation of the person with postal_code not as zero.
        4.1 Invoke the create method of personService and store the returned value in id.
        4.2 Check whether the id is greater than zero.
    5. Priority three is to check the address creation of the person with duplicate mailid.
        5.1 Invoke the create method of personService
        5.2 Check whether it throws the expected AppException.
    6. Priority four is to check the address creation of the person with unique mailid.
        6.1 Invoke the create method of personService and store the returned value in id.
        6.2 Check whether the id is greater than zero.
    7. Priority five  is to check the read method of the personService  with invalid id.
        7.1 Invoke the read method of personService.
        7.2 Check whether the returned value is equal to the expected value.
    8. Priority six is to check the read method of the personService with valid id and boolean flag is false.
        8.1 Invoke the read method of personService and store the returned values in person2.
        8.2 Check whether the returned values of name, email, birthdate, id is equal to the known value.
    9. Priority seven is to check the read method of the personService with valid id and boolean flag is true.
        9.1 Set the address values in person1.
        9.2 Invoke the read method of personService with Address and store the values in person2.
        9.2 Check whether the values of name, email, birthdate,id, street, city, postal_code, id is equal to the known person1 values.
    10. Priority eight is to check the readAll method of personService with address.
        10.1 Invoke the readAll method of personService.
        10.2 Check whether the returned values are not null.
    11. Priority nine is to check the updation of personService with postal_code not as zero and unique mailid.
        11.1 Invoke the update method with person3 and address2
        11.2 Set the values of address2 in person3.
        11.3 Invoke the read method and store the values in person2.
        11.4 Check whether the values of name, email, birthdate,id, street, city, postal_code, id is equal to the known person3 values.
    12. Priority ten is to check the updation of personService with postal_code as zero.
        12.1 Invoke the updation method of personService.
        12.2 Check whether it throws the expected AppException.
    13. Priority eleven is to check the updation of personService with duplicate mailid.
        13.1 Invoke the updation method of personService.
        13.2 Check whether it throws the expected AppException.
    14. Priority twelve is to check the updation of personService with invalid id.
        14.1 Invoke the updation method of personService.
        14.2 Check whether it throws the expected AppException.
    15. Priority thirteen is to check the deletion of personService with valid id.
        15.1 Invoke the delete method of personService.
        15.2 Invoke the read method of the deleted id.
        15.3 Check whether the returned value is equal to the null.
    16. Priority fourteen is to check the deletion of personService with invalid id.
        16.1 Invoke the delete method of personService.
        14.2 Check whether it throws the expected AppException.

Pseudo code:
class PersonServiceTestCase {

    static ConnectionService threadPool = new ConnectionService();

    private PersonService personService = new PersonService();
    private Address address1;
    private Address address2;
    private Person person;
    private Person person1;
    private Person person2;
    private Person person3;
    private Person updatedPerson;
    private Person updatedPerson1;
    private Person expectedPerson;
    private long id;


    @BeforeClass
    public void setup() throws ParseException {

        person = new Person("Abi", "j", "z.gmaail.com", "20-01-2001");
        person1 = new Person("Anu", "j", "a@gamail.com", "20-01-2001");
        person2 = new Person("Aswin", "s", "z.gmail.com", "20-01-2001");
        address1 = new Address("College Road", "Tiruppur", 641605);

        updatedPerson = new Person("Velu", "R", "velu111@gmail.com", "20-10-2000");

        address2 = new Address("Ring Road", "Tiruppur", 641652);

        updatedPerson1 = new Person("Naveen", "A", "naveen@gmail.com", "30-10-2003");
    }

    @Test(groups = "createTest", priority = 1, description = "Person Creation with unique name, unique mail id, without address")
    public void personCreationTest1() {
        new ConnectionService().init();
        this.id = personService.create(person);
        person.setId(id);
        expectedPerson = personService.read(id, false);
        person.setCreatedDate(expectedPerson.getCreatedDate());
        ConnectionService.commit();
        Assert.assertEquals(person.toString(), expectedPerson.toString());
    }

    @Test(groups = "createTest", priority = 2, description = "Person Creation with unique name, unique mail id, with address")
    public void personCreationTest2() {
        new ConnectionService().init();
        person1.setAddress(address1);
        this.id = personService.create(person1);
        person1.setId(id);
        address1.setId(personService.getAddressIdFromPersonId(id, ConnectionService.get()));
        person1.setAddress(address1);
        expectedPerson = personService.read(id, true);
        person1.setCreatedDate(expectedPerson.getCreatedDate());
        ConnectionService.commit();
        Assert.assertEquals(person1.toString(), expectedPerson.toString());
    }

    @Test(groups = "createTest", priority = 3, description = "Person creation with first name and last name as duplicate",
            expectedExceptions = {AppException.class},
            expectedExceptionsMessageRegExp = "ERR421 : First name and last name should not duplicate")
    public void personCreationTest3() {
        person.setAddress(address1);
        personService.create(person);
    }

    @Test(groups = "createTest", priority = 4, description = "Person creation with duplicate email",
            expectedExceptions = AppException.class,
            expectedExceptionsMessageRegExp = "ERR411 : Email should not be unique")
    public void personCreationTest4() throws AppException {
        person.setAddress(address1);
        personService.create(person2);

    }

    @Test(groups = "readTest", priority = 5, description = "Reading person with invalid id")
    public void personReadTest() {
        Assert.assertEquals(personService.read(0, true), null);
    }

    @Test(groups = "readTest", priority = 6, description = "Valid id and the boolean flag is false")
    public void personReadTest2() {
        person3 = personService.read(this.id, false);
        person1.setId(this.id);
        person1.setAddress(null);
        Assert.assertEquals(person3.toString(), person1.toString());
    }

    @Test(groups = "readTest", priority = 7, description = "Valid id and the boolean flag is true")
    public void personReadTest1() {
        threadPool.submit(() -> {
            person1.setAddress(address1);
            System.out.println(Thread.currentThread().getName());
            System.out.println(ConnectionService.get().toString());
            person3 = personService.read(this.id, true);
            person1.setId(23);
            address1.setId(personService.getAddressIdFromPersonId(id, ConnectionService.get()));
            person1.setAddress(address1);
            Assert.assertEquals(person3.toString(), person1.toString());
        });
    }

    @Test(groups = "readAllTest", priority = 8, description = "Reading all persons with address")
    public void personReadAllTest() {

        threadPool.submit(() -> {
            int size = 0;
            System.out.println(Thread.currentThread().getName());
            try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.PERSON_TABLE_SIZE)) {
                ResultSet result = ps.executeQuery();
                if (result.next()) {
                    size = result.getInt("COUNT(*)");
                }
            }catch (Exception e) {
                throw new AppException(ErrorCode.FAILED_TO_GET_ADDRESS_SIZE, e);
            }
            Assert.assertEquals(personService.readAll().size(), size);
        });

    }


    @Test(groups = "updateTest", priority = 9,
            description = "Person updation with duplicate first name and last name",
            expectedExceptions = AppException.class,
            expectedExceptionsMessageRegExp = "ERR421 : first name and last name should be unique")
    public void personUpdationTest() {
        person.setId(this.id);
        person.setAddress(address2);
        personService.update(person);
    }

    @Test(groups = "updateTest", priority = 10, description = "person updation with duplicate email",
            expectedExceptions = AppException.class,
            expectedExceptionsMessageRegExp = "ERR411 : Email should be unique")
    public void personUpdationTest1() {
        person.setId(this.id);
        person.setAddress(address1);
        personService.update(person);
    }

    @Test(groups = "updateTest", priority = 11, description = "Person updation with valid first name, last name, email and address null")
    public void personUpdationTest2() {
        updatedPerson.setId(this.id);
        personService.update(updatedPerson);
        expectedPerson = personService.read(this.id, false);
        expectedPerson.setCreatedDate(null);
        Assert.assertEquals(updatedPerson.toString(), expectedPerson.toString());
    }

    @Test(groups = "updateTest", priority = 12, description = "Person Updation with validname, email and with the new address")
     public void personUpdationTest3() {
        updatedPerson1.setId(this.id);
        updatedPerson1.setAddress(address2);
        personService.update(updatedPerson1);
        expectedPerson = personService.read(this.id, true);
        updatedPerson1.setCreatedDate(expectedPerson.getCreatedDate());
        Assert.assertEquals(updatedPerson1.toString(), expectedPerson.toString());
    }

    @Test(groups = "deleteTest", priority = 13, description = "Deleting person with valid id")
    public void personDeleteTest() {
        personService.delete(this.id);
        ConnectionService.commit();
        Assert.assertEquals(personService.read(this.id, true), null);
    }

    @Test(groups = "deleteTest", priority = 14, description = "Deleting address with invalid id",
            expectedExceptions = AppException.class,
            expectedExceptionsMessageRegExp = "ERR408 : Failed to delete Person")
    public void personDeleteTest1() {
        personService.delete(100);
    }

    @Test(groups = "createTest", priority = 15, description = "Creating 10 persons by reading data fromcsv file")
    public void personCreateCSV() {
        ArrayList<Person> persons = new ArrayList<>();
        PersonServiceTestCase test = new PersonServiceTestCase();
        PersonService ps = new PersonService();
        persons = test.readCsvFile("D:\\File\\person.csv");
        for (Person person : persons) {
            ps.create(person);
            ConnectionService.commit();
        }
    }

    public ArrayList<Person> readCsvFile(String fileName) {

        BufferedReader fileReader = null;
        ArrayList<Person> persons = new ArrayList<>();

        try {

            String line = "";
            fileReader = new BufferedReader(new FileReader(fileName));
            fileReader.readLine();

            while ((line = fileReader.readLine()) != null) {
                String[] tokens = line.split(",");
                if (tokens.length > 0) {
                    Address address = null;
                    Person person = new Person(tokens[1], tokens[2], tokens[3], tokens[4]);
                    if (tokens[5] != "NULL" && tokens[6] != "NULL"
                            && Integer.parseInt(tokens[7]) != 0) {
                        address = new Address(tokens[5], tokens[6], Integer.parseInt(tokens[7]));
                    }

                    person.setAddress(address);
                    persons.add(person);
                }
            }
        } catch (Exception e) {
            System.out.println("Error in CsvFileReader !");
            e.printStackTrace();
        } finally {
            try {
                fileReader.close();
            } catch (IOException e) {
                System.out.println("Error while closing fileReader !!!");
                e.printStackTrace();
            }
        }
        return persons;
    }
}
 */

package in.kpr.training.jdbc.testcases;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import in.kpr.training.jdbc.exception.AppException;
import in.kpr.training.jdbc.model.Address;
import in.kpr.training.jdbc.model.Person;
import in.kpr.training.jdbc.services.ConnectionService;
import in.kpr.training.jdbc.services.PersonService;

public class PersonServiceTestCases {

    static ConnectionService threadPool = new ConnectionService();

    private PersonService personService = new PersonService();
    private Address address1;
    private Address address2;
    private Person person;
    private Person person1;
    private Person person2;
    private Person person3;
    private Person updatedPerson;
    private Person updatedPerson1;
    private Person expectedPerson;
    private long id;


    @BeforeClass
    public void setup() throws ParseException {

        person = new Person("Abi", "j", "z.gmaail.com", "20-01-2001");
        person1 = new Person("Anu", "j", "a@gamail.com", "20-01-2001");
        person2 = new Person("Aswin", "s", "z.gmail.com", "20-01-2001");
        address1 = new Address("College Road", "Tiruppur", 641605);

        updatedPerson = new Person("Velu", "R", "velu111@gmail.com", "20-10-2000");

        address2 = new Address("Ring Road", "Tiruppur", 641652);

        updatedPerson1 = new Person("Naveen", "A", "naveen@gmail.com", "30-10-2003");
    }

    @Test(groups = "createTest", priority = 1, description = "Person Creation with unique name, unique mail id, without address")
    public void personCreationTest1() {
        ConnectionService.init();
        this.id = personService.create(person);
        person.setId(id);
        expectedPerson = personService.read(id, false);
        person.setCreatedDate(expectedPerson.getCreatedDate());
        Assert.assertEquals(person.toString(), expectedPerson.toString());
    }

    @Test(groups = "createTest", priority = 2, description = "Person Creation with unique name, unique mail id, with address")
    public void personCreationTest2() {
        ConnectionService.init();
        person1.setAddress(address1);
        this.id = personService.create(person1);
        person1.setId(id);
        address1.setId(personService.getAddressIdByPersonId(id, ConnectionService.get()));
        person1.setAddress(address1);
        expectedPerson = personService.read(id, true);
        person1.setCreatedDate(expectedPerson.getCreatedDate());
        Assert.assertEquals(person1.toString(), expectedPerson.toString());
    }

    @Test(groups = "createTest", priority = 3, description = "Person creation with first name and last name as duplicate",
            expectedExceptions = {AppException.class},
            expectedExceptionsMessageRegExp = "ERR421 : First name and last name should not duplicate")
    public void personCreationTest3() {
        person.setAddress(address1);
        personService.create(person);
    }

    @Test(groups = "createTest", priority = 4, description = "Person creation with duplicate email",
            expectedExceptions = AppException.class,
            expectedExceptionsMessageRegExp = "ERR411 : Email should not be unique")
    public void personCreationTest4() throws AppException {
        person.setAddress(address1);
        personService.create(person2);

    }

    @Test(groups = "readTest", priority = 5, description = "Reading person with invalid id")
    public void personReadTest() {
        Assert.assertEquals(personService.read(0, true), null);
    }

    @Test(groups = "readTest", priority = 6, description = "Valid id and the boolean flag is false")
    public void personReadTest2() {
        person3 = personService.read(this.id, false);
        person1.setId(this.id);
        person1.setAddress(null);
        Assert.assertEquals(person3.toString(), person1.toString());
    }

    @Test(groups = "readTest", priority = 7, description = "Valid id and the boolean flag is true")
    public void personReadTest1() {
        person1.setAddress(address1);
        System.out.println(Thread.currentThread().getName());
        System.out.println(ConnectionService.get().toString());
        person3 = personService.read(this.id, true);
        person1.setId(23);
        address1.setId(personService.getAddressIdByPersonId(id, ConnectionService.get()));
        person1.setAddress(address1);
        Assert.assertEquals(person3.toString(), person1.toString());
    }

    @Test(groups = "readAllTest", priority = 8, description = "Reading all persons with address")
    public void personReadAllTest() {

        Assert.assertNull(personService.readAll().size());        
    }


    @Test(groups = "updateTest", priority = 9,
            description = "Person updation with duplicate first name and last name",
            expectedExceptions = AppException.class,
            expectedExceptionsMessageRegExp = "ERR421 : first name and last name should be unique")
    public void personUpdationTest() {
        person.setId(this.id);
        person.setAddress(address2);
        personService.update(person);
    }

    @Test(groups = "updateTest", priority = 10, description = "person updation with duplicate email",
            expectedExceptions = AppException.class,
            expectedExceptionsMessageRegExp = "ERR411 : Email should be unique")
    public void personUpdationTest1() {
        person.setId(this.id);
        person.setAddress(address1);
        personService.update(person);
    }

    @Test(groups = "updateTest", priority = 11, description = "Person updation with valid first name, last name, email and address null")
    public void personUpdationTest2() {
        updatedPerson.setId(this.id);
        personService.update(updatedPerson);
        expectedPerson = personService.read(this.id, false);
        expectedPerson.setCreatedDate(null);
        Assert.assertEquals(updatedPerson.toString(), expectedPerson.toString());
    }

    @Test(groups = "updateTest", priority = 12, description = "Person Updation with validname, email and with the new address")
    public void personUpdationTest3() {
        updatedPerson1.setId(this.id);
        updatedPerson1.setAddress(address2);
        personService.update(updatedPerson1);
        expectedPerson = personService.read(this.id, true);
        updatedPerson1.setCreatedDate(expectedPerson.getCreatedDate());
        Assert.assertEquals(updatedPerson1.toString(), expectedPerson.toString());
    }

    @Test(groups = "deleteTest", priority = 13, description = "Deleting person with valid id")
    public void personDeleteTest() {
        personService.delete(this.id);
        Assert.assertEquals(personService.read(this.id, true), null);
    }

    @Test(groups = "deleteTest", priority = 14, description = "Deleting address with invalid id",
            expectedExceptions = AppException.class,
            expectedExceptionsMessageRegExp = "ERR408 : Failed to delete Person")
    public void personDeleteTest1() {
        personService.delete(100);
    }

    @Test(groups = "createTest", priority = 15, description = "Creating 10 persons by reading data fromcsv file")
    public void personCreateCSV() {
        ArrayList<Person> persons = new ArrayList<>();
        PersonServiceTestCases test = new PersonServiceTestCases();
        PersonService ps = new PersonService();
        persons = test.readCsvFile("D:\\File\\person.csv");
        for (Person person : persons) {
            ps.create(person);
        }
    }

    public ArrayList<Person> readCsvFile(String fileName) {

        BufferedReader fileReader = null;
        ArrayList<Person> persons = new ArrayList<>();

        try {

            String line = "";
            fileReader = new BufferedReader(new FileReader(fileName));
            fileReader.readLine();

            while ((line = fileReader.readLine()) != null) {
                String[] tokens = line.split(",");
                if (tokens.length > 0) {
                    Address address = null;
                    Person person = new Person(tokens[1], tokens[2], tokens[3], tokens[4]);
                    if (tokens[5] != "NULL" && tokens[6] != "NULL"
                            && Integer.parseInt(tokens[7]) != 0) {
                        address = new Address(tokens[5], tokens[6], Integer.parseInt(tokens[7]));
                    }

                    person.setAddress(address);
                    persons.add(person);
                }
            }
        } catch (Exception e) {
            System.out.println("Error in CsvFileReader !");
            e.printStackTrace();
        } finally {
            try {
                fileReader.close();
            } catch (IOException e) {
                System.out.println("Error while closing fileReader !!!");
                e.printStackTrace();
            }
        }
        return persons;
    }
}
